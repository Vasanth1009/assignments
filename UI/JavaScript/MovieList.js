 var moviedata;
            var movieElement;
            //Init Function
            $(function(){
                getMovieList();
            });

            function getMovieList(){
                movieElement = $("#moviesList");
                $.ajax({
                    type: 'GET',
                    datatype: 'json',
                    url: movieURL,
                    success: function (data) {
                        moviedata=data;
                        renderMovieList();
                    }
                });
            }; 

            function renderMovieList(){
                for (var i = 0; i < moviedata.length; i++) {
                    var movie = moviedata[i];
                    movieElement.append(
                        '<div class="col">' +
                            '<div class="card shadow-sm" id="movie">' +
                                '<img class="bd-placeholder-img card-img-top" data-bs-toggle="modal" data-bs-target="#Modal'+ movie.id +'" src="' + movie.poster + '" width="100%" height="250px" alt="Not Found" onerror=this.src="Images/IMDb.png">' +
                                '<div class="card-body">' +
                                    '<h4 class="card-text" id="movieName"> <span style="cursor:pointer;" data-bs-toggle="modal" data-bs-target="#Modal'+ movie.id +'">' + movie.name + '</span></h4>' + 
                                    '<div class="d-flex justify-content-between align-items-center">' +
                                        '<small class="movie-year text-muted">'+ movie.yearOfRelease +'</small>' +
                                        '<div class="movie-button">' +
                                            // '<button type="button" class="btn btn-sm me-2 btn-outline-success" data-bs-toggle="modal" data-bs-target="#Modal'+i+'">View </button>' +
                                            '<a href="MovieForm.html?id='+ movie.id +'"><button type="button" class="btn btn-sm me-2 btn-outline-primary" onclick="storeMovie(' + i + ')"> <i class="fas fa-edit"></i> Edit </button></a>' +
                                            '<button type="button" class="btn btn-sm btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deleteMovieModal'+movie.id+'"><i class="fas fa-trash-alt"></i> Delete</button>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +

                        '<div class="modal fade" id="Modal'+ movie.id +'" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                            '<div class="modal-dialog">' +
                                '<div class="modal-content">' +
                                    '<div class="modal-header">' +
                                        '<h5 class="modal-title" id="exampleModalLabel">' + movie.name + '</h5>' +
                                        '<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                        '<h5>Plot: </h5>' +
                                        '<p>' +
                                            movie.plot +
                                        '</p>' +
                                        '<hr>' +
                                        '<p> <strong>Producer: </strong>' + movie.producer.name +'</p>' +
                                        '<p> <strong>Actor: </strong> <span id='+'actor_'+ movie.id +'></span></p>' +
                                        '<p> <strong>Genre: </strong> <span id='+'genre_'+ movie.id +'></span></p>'+
                                    '</div>' +
                                    '<div class="modal-footer">' +
                                        '<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +

                        ' <div class="modal fade" id="deleteMovieModal'+ movie.id +'" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                            ' <div class="modal-dialog">' +
                                ' <div class="modal-content">' +
                                ' <div class="modal-header">' +
                                    ' <h5 class="modal-title" id="exampleModalLabel">Delete movie</h5>' +
                                    ' <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>' +
                                ' </div>' +
                                ' <div class="modal-body">' +
                                ' <h5> Are you sure to want to delete this movie? </h5> '+
                                ' </div>' +
                                ' <div class="modal-footer">' +
                                '  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>' +
                                    ' <button type="button" class="btn btn-primary" onclick="deleteMovie(\'' + movie.id + '\')">Delete</button>' +
                                ' </div>' +
                                ' </div>' +
                            ' </div>' +
                        ' </div>' 
                    );
                    for(var j = 0;j<movie.actors.length;j++)
                        { $("#actor_"+movie.id).append(movie.actors[j].name + ((j<movie.actors.length-1) ? ', ':'') );                                                    
                    }
                    for(var j = 0;j<movie.genres.length;j++)
                        { $("#genre_"+movie.id).append(movie.genres[j].name + ((j<movie.genres.length-1) ? ', ':'') );                                                    
                    }
                    
                }
            }
    

            // Delete Movie
            function deleteMovie(movieId){
                  $.ajax({
                    type: 'DELETE',
                    datatype: 'json',
                    url: movieURL + movieId ,
                    success: function () {
                        movieElement.empty();
                        var removeIndex = moviedata.map(movie => movie.id).indexOf(JSON.parse(movieId));
                        ~removeIndex && moviedata.splice(removeIndex, 1);
                        renderMovieList();
                    }
                });
                $('#deleteMovieModal'+ movieId).modal('hide');
            }
            
            function storeMovie(index) {
                localStorage.setItem('list', JSON.stringify(moviedata[index]));
            }