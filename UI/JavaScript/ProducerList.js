   
            var producerdata;
            var producerElement;
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "preventDuplicates": true,
                "positionClass": "toast-bottom-left",
                "onclick": null,
                "showDuration": "100",
                "hideDuration": "100",
                "timeOut": "1500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "show",
                "hideMethod": "hide"
            };

            //Init Function
            $(function(){
                getProducerList();
            });

            // Get Producer
            function getProducerList(){
                producerElement = $("#producersList");
                $.ajax({
                    type: 'GET',
                    datatype: 'json',
                    url: producerURL,
                    success: function (data) {
                        producerdata=data;
                        renderProducerList();
                    }
                });
            };
             
            // Add Producer
            function addProducer(){
                var closeProducer = false;
                var name = $("#producer-name").val();
                var gender = $("#gender").val();
                var bio = $("#bio-text").val();
                var dob = $("#dob").val();

                if (name === "" || gender === "" || bio === "" || dob === "") {
                    closeProducer = false;
                }
                else{
                    closeProducer = true;
                }

                var producersList={
                    name: name,
                    gender:gender,
                    bio: bio,
                    dob: dob
                };

                if(closeProducer){
                    $.ajax({
                        type: 'POST',
                        url: producerURL,
                        contentType: 'application/json',
                        data: JSON.stringify(producersList),
                        success:function(data){
                            producerdata.push(data);
                            producerElement.empty();
                            renderProducerList();
                            clearForm();
                            $('#producerModal').modal('hide');
                            toastr.success('Producer Added Successfully');
                        },
                    });
                }
                else{
                    toastr.error('Something went wrong');
                }
                closeProducer= false;
            };

            // Edit Producer
            function editProducer(producerId){
                var closeProducer = false;
                var name = $("#producer-name").val();
                var gender = $("#gender").val();
                var bio = $("#bio-text").val();
                var dob = $("#dob").val();

                if (name === "" || gender === "" || bio === "" || dob=== "") {
                    closeProducer = false;
                }
                else{
                    closeProducer = true;
                }

                var producersList={
                    name: name,
                    gender:gender,
                    bio: bio,
                    dob: dob
                };

                if(closeProducer) {
                    $.ajax({
                        type: 'PUT',
                        url: producerURL + producerId ,
                        contentType: 'application/json',
                        data: JSON.stringify(producersList),
                        success: function(){
                            producerElement.empty();
                            getProducerList();
                            clearForm();
                            toastr.success('Producer updated Successfully');
                        }, 
                    });
                    $('#producerModal').modal('hide');
                }
                else{
                    toastr.error('Something went wrong');
                }
                closeProducer = false;
            };

            
            // Delete Producer
            function deleteProducer(producerId){
                  $.ajax({
                    type: 'DELETE',
                    datatype: 'json',
                    url: producerURL + producerId ,
                    success: function () {
                        producerElement.empty();
                        var removeIndex = producerdata.map(producer => producer.id).indexOf(JSON.parse(producerId));
                        ~removeIndex && producerdata.splice(removeIndex, 1);
                        renderProducerList();
                    }
                });
                $('#deleteProducerModal'+producerId).modal('hide');
            }

            // Render List
            function renderProducerList(){
                for (var i = 0; i < producerdata.length; i++) {
                    var producer = producerdata[i];
                    producerElement.append(
                        '<div class="col">' +
                            '<div class="card shadow-sm" id="producer">' +
                                '<div class="card-body">' +
                                    '<div class="d-flex justify-content-between align-items-center">' +
                                        '<h4 class="card-text" id="producerName"> <span style="cursor:pointer;" data-bs-toggle="modal" data-bs-target="#Modal'+ producer.id +'">' + producer.name + '</span></h4>' + 
                                        '<div class="producer-button">' +
                                            '<button type="button" class="btn btn-sm me-2 btn-outline-primary" data-bs-toggle="modal" data-bs-target="#producerModal" onclick="editForm(\'' + i + '\')"> <i class="fas fa-edit"></i> Edit </button>' +
                                            '<button type="button" class="btn btn-sm btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deleteProducerModal'+producer.id+'"><i class="fas fa-trash-alt"></i> Delete</button>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +

                        '<div class="modal fade" id="Modal'+ producer.id +'" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                            '<div class="modal-dialog">' +
                                '<div class="modal-content">' +
                                    '<div class="modal-header">' +
                                        '<h5 class="modal-title" id="exampleModalLabel">' + producer.name + '</h5>' +
                                        '<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                        '<p> <strong>Genre:</strong> '+ producer.gender + '</p>' +
                                        '<p> <strong>DOB:</strong> '+ producer.dob.split('T')[0] + '</p>' +
                                        '<p> <strong>Bio:</strong> '+ producer.bio + '</p>' +
                                    '</div>' +
                                    '<div class="modal-footer">' +
                                        '<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +

                        ' <div class="modal fade" id="deleteProducerModal'+ producer.id +'" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                            ' <div class="modal-dialog">' +
                                ' <div class="modal-content">' +
                                ' <div class="modal-header">' +
                                    ' <h5 class="modal-title" id="exampleModalLabel">Delete Producer</h5>' +
                                    ' <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>' +
                                ' </div>' +
                                ' <div class="modal-body">' +
                                   ' <h5> Are you sure to want to delete this producer? </h5> '+
                                ' </div>' +
                                ' <div class="modal-footer">' +
                                   '  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>' +
                                    ' <button type="button" class="btn btn-primary" onclick="deleteProducer(\'' + producer.id + '\')">Delete</button>' +
                                ' </div>' +
                                ' </div>' +
                            ' </div>' +
                        ' </div>' 
                    );
                }
            }

            function editForm(index){
                var data=producerdata[index];
                console.log(data.id)
                $('#producerModal').find('.modal-header').empty().append(
                    '<h5 class="modal-title" id="exampleModalLabel">Edit Producer</h5>' +
                    '<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'
                );
                $('#producerModal').find('.modal-footer').empty().append(   
                    '<button type="submit" class="btn btn-primary" onclick="editProducer(\''+ data.id +'\')">Update Producer</button>'
                );
                $("#producer-name").val(data.name);
                $("#gender").val(data.gender);
                $("#bio-text").val(data.bio);
                $("#dob").val(data.dob.split('T')[0]);
            }

            // Reset Form 
            function clearForm() {
                $('#producerForm')[0].classList.remove("was-validated");
                $("#producer-name").val("");
                $("#gender").val("");
                $("#bio-text").val("");
                $("#dob").val("");
            }

            function openProducerModal(){
                clearForm();
                $('.modal-header').empty().append(
                    '<h5 class="modal-title" id="exampleModalLabel">Add Producer</h5>' +
                    '<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'
                );
                $('.modal-footer').empty().append(   
                    '<button type="submit" class="btn btn-primary" onclick="addProducer()">Add Producer</button>'
                );
            }

             // FormValidation
           (function () {
                'use strict'
                var forms  = document.querySelectorAll('.needs-validation')
                Array.prototype.slice.call(forms)
                    .forEach(function (form) {
                        form.addEventListener('submit', function (event) {
                            if (!form.checkValidity()) {
                                event.preventDefault()
                                event.stopPropagation()
                            }
                            form.classList.add('was-validated')
                        }, false)
                    })
            })()