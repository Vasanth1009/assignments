
            var actordata;
            var actorElement;
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "preventDuplicates": true,
                "positionClass": "toast-bottom-left",
                "onclick": null,
                "showDuration": "100",
                "hideDuration": "100",
                "timeOut": "1500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "show",
                "hideMethod": "hide"
            };

             //Init Function
            $(function(){
                getActorList();

            });

            // Get Actor
            function getActorList(){
                actorElement = $("#actorsList");
                $.ajax({
                    type: 'GET',
                    datatype: 'json',
                    url: actorURL,
                    success: function (data) {
                        actordata=data;
                        renderActorList();
                    }
                });
            }; 

            // Add Actor
            function addActor(){
                
                var closeActor = false;
                var name = $("#actor-name").val();
                var gender = $("#gender").val();
                var bio = $("#bio-text").val();
                var dob = $("#dob").val();

                if (name === "" || gender === "" || bio === "" || dob=== "") {
                    closeActor = false;
                }
                else{
                    closeActor = true;
                }

                var actorsList={
                    name: name,
                    gender:gender,
                    bio: bio,
                    dob: dob
                };

                if(closeActor) {
                    $.ajax({
                        type: 'POST',
                        url: actorURL,
                        contentType: 'application/json',
                        data: JSON.stringify(actorsList),
                        success: function(data){
                            actordata.push(data);
                            actorElement.empty();
                            renderActorList();
                            clearForm();
                            toastr.success('Actor Added Successfully');
                        },
                    });
                    $('#actorModal').modal('hide');
                }
                else{
                    toastr.error('Something went wrong');
                }
                closeActor = false;
            };

            // Edit Actor
            function editActor(actorId){
                var closeActor = false;
                var name = $("#actor-name").val();
                var gender = $("#gender").val();
                var bio = $("#bio-text").val();
                var dob = $("#dob").val();

                if (name === "" || gender === "" || bio === "" || dob=== "") {
                    closeActor = false;
                }
                else{
                    closeActor = true;
                }

                var actorsList={
                    name: name,
                    gender:gender,
                    bio: bio,
                    dob: dob
                };

                if(closeActor) {
                    $.ajax({
                        type: 'PUT',
                        url: actorURL + actorId ,
                        contentType: 'application/json',
                        data: JSON.stringify(actorsList),
                        success: function(){
                            actorElement.empty();
                            getActorList();
                            clearForm();
                            toastr.success('Actor updated Successfully');
                        }, 
                    });
                    $('#actorModal').modal('hide');
                }
                else{
                    toastr.error('Something went wrong');
                }
                closeActor = false;
            };

            // Delete Actor
            function deleteActor(actorId){
                  $.ajax({
                    type: 'DELETE',
                    datatype: 'json',
                    url: actorURL + actorId ,
                    success: function () {
                        actorElement.empty();
                        var removeIndex = actordata.map(actor => actor.id).indexOf(JSON.parse(actorId));
                        ~removeIndex && actordata.splice(removeIndex, 1);
                        renderActorList();
                    }
                });
                $('#deleteActorModal'+actorId).modal('hide');
            }

            // Render List
            function renderActorList() {
                for (var i = 0; i < actordata.length; i++) {
                    var actor = actordata[i];
                    actorElement.append(
                        '<div class="col">' +
                            '<div class="card shadow-sm" id="actor">' +
                                '<div class="card-body">' +
                                    '<div class="d-flex justify-content-between align-items-center">' +
                                        '<h4 class="card-text" id="actorName"> <span style="cursor:pointer;" data-bs-toggle="modal" data-bs-target="#Modal'+ actor.id +'">' + actor.name + '</span></h4>' + 
                                        '<div class="actor-button">' +
                                            '<button type="button" class="btn btn-sm me-2 btn-outline-primary" data-bs-toggle="modal" data-bs-target="#actorModal" onclick="editForm(\'' + i + '\')"> <i class="fas fa-edit"></i> Edit </button>' +
                                            '<button type="button" class="btn btn-sm btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deleteActorModal'+actor.id+'"><i class="fas fa-trash-alt"></i> Delete</button>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +

                        '<div class="modal fade" id="Modal'+ actor.id +'" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                            '<div class="modal-dialog">' +
                                '<div class="modal-content">' +
                                    '<div class="modal-header">' +
                                        '<h5 class="modal-title" id="exampleModalLabel">' + actor.name + '</h5>' +
                                        '<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                        '<p> <strong>Genre:</strong> '+ actor.gender + '</p>' +
                                        '<p> <strong>DOB:</strong> '+ actor.dob.split('T')[0] + '</p>' +
                                        '<p> <strong>Bio:</strong> '+ actor.bio + '</p>' +
                                    '</div>' +
                                    '<div class="modal-footer">' +
                                        '<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +

                        ' <div class="modal fade" id="deleteActorModal'+ actor.id +'" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">' +
                            ' <div class="modal-dialog">' +
                                ' <div class="modal-content">' +
                                ' <div class="modal-header">' +
                                    ' <h5 class="modal-title" id="exampleModalLabel">Delete Actor</h5>' +
                                    ' <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>' +
                                ' </div>' +
                                ' <div class="modal-body">' +
                                   ' <h5> Are you sure to want to delete this actor? </h5> '+
                                ' </div>' +
                                ' <div class="modal-footer">' +
                                   '  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>' +
                                    ' <button type="button" class="btn btn-primary" onclick="deleteActor(\'' + actor.id + '\')">Delete</button>' +
                                ' </div>' +
                                ' </div>' +
                            ' </div>' +
                        ' </div>' 
                    );
                }
            }

            function editForm(index){
                var data=actordata[index];
                $('#actorModal').find('.modal-header').empty().append(
                    '<h5 class="modal-title" id="exampleModalLabel">Edit Actor</h5>' +
                    '<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'
                );
                $('#actorModal').find('.modal-footer').empty().append(   
                    '<button type="submit" class="btn btn-primary" onclick="editActor(\''+ data.id +'\')">Update Actor</button>'
                );
                $("#actor-name").val(data.name);
                $("#gender").val(data.gender);
                $("#bio-text").val(data.bio);
                $("#dob").val(data.dob.split('T')[0]);
            }
            
            // Clear Form
            function clearForm() {
                $('#actorForm')[0].classList.remove("was-validated");
                $("#actor-name").val("");
                $("#gender").val("");
                $("#bio-text").val("");
                $("#dob").val("");
            }

            function openActorModal(){
                clearForm();
                $('.modal-header').empty().append(
                    '<h5 class="modal-title" id="exampleModalLabel">Add Actor</h5>' +
                    '<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'
                );
                $('.modal-footer').empty().append(   
                    '<button type="submit" class="btn btn-primary" onclick="addActor()">Add Actor</button>'
                );
            }

            // FormValidation
            (function () {
                'use strict'
                var forms = document.querySelectorAll('.needs-validation')
                Array.prototype.slice.call(forms)
                    .forEach(function (form) {
                        form.addEventListener('submit', function (event) {
                            if (!form.checkValidity()) {
                                event.preventDefault()
                                event.stopPropagation()
                            }
                            form.classList.add('was-validated')
                        }, false)
                    })
            })() 
