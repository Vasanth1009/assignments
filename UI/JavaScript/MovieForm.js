  var actorElement;
            var producerElement;
            var genreElement;
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "preventDuplicates": true,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "100",
                "hideDuration": "100",
                "timeOut": "1500",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "show",
                "hideMethod": "hide"
            };
            
            // DatePicker
            $('#yearOfRelease').datepicker({
                format: "yyyy",
                minViewMode: "years",
                startDate:'1900',
                endDate:'2021',
                autoclose: true
            }); 

            // DropDownSelection
            $(document).ready(function() {
                $('.js-example-basic-multiple-actors').select2({
                    placeholder: "Select a actors"
                });
                $('.js-example-basic-multiple-genres').select2({
                    placeholder: "Select a genres"
                });
                $(".js-example-basic-single").select2({
                    placeholder: "Select a producer",
                });
                $(".js-example-responsive").select2({
                    width: 'resolve'
                });
            });
            
            //Init Function
            $(function(){
                getActorList();
                getProducerList();
                getGenreList();
            });
            
            // GetActors
            function getActorList() {
                actorElement = $('#actors');
                $.ajax({
                    type: 'GET',
                    datatype: 'json',
                    url: actorURL,
                    success: function (data) {
                        $.each(data, function (index, data) {
                            actorElement.append(
                                "<option value='" + data.id + "'>" + data.name + '</option>'
                                );
                            });
                    },
                });
            };
        
            // GetProducers
            function getProducerList() {
            producerElement = $('#producers');
                $.ajax({
                    type: 'GET',
                    datatype: 'json',
                    url: producerURL,
                    success: function (data) {
                        $.each(data, function (index, data) {
                            producerElement.append(
                                "<option value='" + data.id + "''>" + data.name + '</option>'
                            );
                        });
                    },
                });
            };

            // GetGenres
            function getGenreList() {
            genreElement = $('#genres');
                $.ajax({
                    type: 'GET',
                    datatype: 'json',
                    url: genreURL,
                    success: function (data) {
                        $.each(data, function (index, data) {
                            genreElement.append(
                                "<option value='" + data.id + "'>" + data.name + '</option>'
                            );
                        });
                    },
                });
            };

            //Add Actor
            function addActor(){
                var closeActor = false;
                var name = $("#actor-name").val();
                var gender = $("#actor-gender").val();
                var bio = $("#actor-bio").val();
                var dob = $("#actor-dob").val();

                if (name === "" || gender === "" || bio === "" || dob === "") {
                    closeActor = false;
                }
                else{
                    closeActor = true;
                }

                var actorsList={
                    name: name,
                    gender:gender,
                    bio: bio,
                    dob: dob
                };

                if(closeActor) {
                    $.ajax({
                        type: 'POST',
                        url: actorURL,
                        contentType: 'application/json',
                        data: JSON.stringify(actorsList),
                        success:function(data){
                            actorElement.append(
                                '<option value="' + data.id + '">' + data.name + '</option>'
                            );
                            clearActorForm();
                            toastr.success('Actor Added Successfully');
                        }, 
                    });
                    $('#actorModal').modal('hide');
                }
                else{
                    toastr.error('Something went wrong');
                }
                closeActor = false;
            };

            // Add Producer
            function addProducer(){
                var closeProducer = false;
                var name = $("#producer-name").val();
                var gender = $("#producer-gender").val();
                var bio = $("#producer-bio").val();
                var dob = $("#producer-dob").val();

                if (name === "" || gender === "" || bio === "" || dob === "") {
                    closeProducer = false;
                }
                else{
                    closeProducer = true;
                }
                console.log(closeProducer)
                var producersList={
                    name: name,
                    gender:gender,
                    bio: bio,
                    dob: dob
                };

                if(closeProducer){
                    $.ajax({
                        type: 'POST',
                        url: producerURL,
                        contentType: 'application/json',
                        data: JSON.stringify(producersList),
                        success:function(data){
                            producerElement.append(
                                '<option value="' + data.id + '">' + data.name + '</option>'
                            );
                            clearProducerForm();
                            toastr.success('Producer Added Successfully');
                        },
                    });
                    $('#producerModal').modal('hide');
                }
                else{
                    toastr.error('Something went wrong');
                }
                closeProducer= false;
            };

            // Add Genre
            function addGenre(){
                var closeGenre = false;
                var name = $("#genre-name").val();

                if (name === "") {
                    closeGenre = false;
                }
                else{
                    closeGenre = true;
                }

                var genresList={
                    name: name,
                };

                if(closeGenre){
                    $.ajax({
                        type: 'POST',
                        url: genreURL,
                        contentType: 'application/json',
                        data: JSON.stringify(genresList),
                        success:function(data){
                            console.log(data)
                             genreElement.append(
                                '<option value="' + data.id + '">' + data.name + '</option>'
                            );
                            clearGenreForm();
                            toastr.success('Genre Added Successfully');
                        },
                    });
                    $('#genreModal').modal('hide');
                }
                else{
                    toastr.error('Something went wrong');
                }
                closeGenre= false;
            };

            // Add Movie 
            function addMovie() {

                var formData = new FormData();
                var imageFile = $("#poster")[0].files;
                formData.append('file', imageFile[0]);
                $.ajax({
                  type: 'POST',
                  url: movieURL + 'upload',
                  data: formData,
                  enctype: 'multipart/form-data',
                  processData: false,
                  contentType: false,
                  success: function (imageUrl) {
                    var closeMovie = false;
                    var name = $('#movie-name').val();
                    var yearOfRelease = $('#yearOfRelease').val();
                    var plot = $('#plot').val();
                    var poster = imageUrl;
                    var actors = $('#actors').val();
                    var actorsId = actors.map((i) => Number(i));
                    var producer = $('#producers').val();
                    var producerId = producer[0];
                    var genres = $('#genres').val();
                    var genresId = genres.map((i) => Number(i));
                    if (
                      name === '' ||
                      yearOfRelease === '' ||
                      plot === '' ||
                      poster === '' ||
                      actorsId.length === 0 ||
                      producer.length === 0 ||
                      genresId.length === 0
                    ) {
                      closeMovie = false;
                    } else {
                      closeMovie = true;
                    }
                    var moviesList = {
                      name: name,
                      yearOfRelease: yearOfRelease,
                      plot: plot,
                      producerId: producerId,
                      poster: poster,
                      actorsId: actorsId,
                      genresId: genresId,
                    };

                    console.log(moviesList);
                    if (closeMovie) {
                      $.ajax({
                        type: 'POST',
                        url: movieURL,
                        contentType: 'application/json',
                        data: JSON.stringify(moviesList),
                        success: function () {
                          clearMovieForm();
                          toastr.success('Movie Added Successfully');
                          setTimeout(function () {
                            location.href = 'MovieList.html';
                          }, 1500);
                        },
                      });
                    } else {
                      toastr.error('Something went wrong');
                    }
                    closeMovie = false;
                  },
                });
            }

            // Edit Movie
            function editMovie(movieId) {
                var formData = new FormData();
                var imageFile = $('#poster')[0].files;
                formData.append('file', imageFile[0]);

                var image;
                $.ajax({
                  type: 'POST',
                  url: movieURL + 'upload',
                  data: formData,
                  enctype: 'multipart/form-data',
                  processData: false,
                  contentType: false,
                  success: function (imageUrl) {
                    var closeMovie = false;
                    console.log(movieId);
                    var name = $('#movie-name').val();
                    var yearOfRelease = $('#yearOfRelease').val();
                    var plot = $('#plot').val();
                    var poster = imageUrl;
                    var actors = $('#actors').val();
                    var actorsId = actors.map((i) => Number(i));
                    var producer = $('#producers').val();
                    var producerId = producer[0];
                    var genres = $('#genres').val();
                    var genresId = genres.map((i) => Number(i));
                    if (
                      name === '' ||
                      yearOfRelease === '' ||
                      plot === '' ||
                      poster === '' ||
                      actorsId.length === 0 ||
                      producer.length === 0 ||
                      genresId.length === 0
                    ) {
                      closeMovie = false;
                    } else {
                      closeMovie = true;
                    }
                    var moviesList = {
                      name: name,
                      yearOfRelease: yearOfRelease,
                      plot: plot,
                      producerId: producerId,
                      poster: poster,
                      actorsId: actorsId,
                      genresId: genresId,
                    };

                    console.log(moviesList);
                    if (closeMovie) {
                      $.ajax({
                        type: 'PUT',
                        url: movieURL + movieId,
                        contentType: 'application/json',
                        data: JSON.stringify(moviesList),
                        success: function () {
                          clearMovieForm();
                          toastr.success('Movie Updated Successfully');
                          setTimeout(function () {
                            location.href = 'MovieList.html';
                          }, 1500);
                        },
                      });
                    } else {
                      toastr.error('Something went wrong');
                    }
                    closeMovie = false;
                  },
                });

                
            }

            // Get Movie By Id 
            $(document).ready(function () {
                var id = getUrlVars()["id"];
                if(id !== undefined){
                    var getlist = localStorage.getItem('list');
                    var values = JSON.parse(getlist);
                    if( values != null ){
                        $("#movie-name").val(values.name);
                        $("#yearOfRelease").val(values.yearOfRelease);
                        $("#plot").val(values.plot);
                        var producerOptions = new Option(values.producer.name, values.producer.id, true, true);
                        $('#producers').append(producerOptions).trigger('change');
                        for(var i=0;i<values.actors.length;i++) {
                            var actorOptions = new Option(values.actors[i].name, values.actors[i].id, true, true);
                            $('#actors').append(actorOptions).trigger('change');
                        }
                        for(var i=0;i<values.genres.length;i++) {
                            var genreOptions = new Option(values.genres[i].name, values.genres[i].id, true, true);
                            $('#genres').append(genreOptions).trigger('change');
                        }
                    }
                    // localStorage.removeItem('list');
                    $('#movie-footer').append(
                        '<button class="w-100 mt-5 btn btn-primary btn-lg" id="addMovieButton" type="submit" onclick="editMovie('+id+')">Update Movie</button>'
                    );
                }
                else
                {
                    $('#movie-footer').append(
                        '<button class="w-100 mt-5 btn btn-primary btn-lg" id="addMovieButton" type="submit" onclick="addMovie()">Add Movie</button>'
                    ); 
                }
            });

            // Get Id By Url
            function getUrlVars()
            {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for(var i = 0; i < hashes.length; i++)
                {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
            }

            // Reset Movie Form
            function clearMovieForm() {
                $('#movieForm')[0].classList.remove("was-validated");
                $("#movie-name").val("");
                $("#yearOfRelease").val("");
                $("#plot").val("");
                $("#poster").val("");
                $("#producers").empty().trigger('change')
                $("#actors").empty().trigger('change')
                $("#genres").empty().trigger('change')
            }

            // Reset Actor Form
            function clearActorForm() {
                $('#actorForm')[0].classList.remove("was-validated");
                $("#actor-name").val("");
                $("#actor-gender").val("");
                $("#actor-bio").val("");
                $("#actor-dob").val("");
            }

            // Reset Producer Form
            function clearProducerForm() {
                $('#producerForm')[0].classList.remove("was-validated");
                $("#producer-name").val("");
                $("#producer-gender").val("");
                $("#producer-bio").val("");
                $("#producer-dob").val("");
            }

            // Reset GenreForm
            function clearGenreForm() {
                $('#genreForm')[0].classList.remove("was-validated");
                $("#genre-name").val("");
            }

            // FormValidation
            (function () {
                'use strict'
                var forms = document.querySelectorAll('.needs-validation')
                Array.prototype.slice.call(forms)
                    .forEach(function (form) {
                        form.addEventListener('submit', function (event) {
                            if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                            }

                            form.classList.add('was-validated')
                        }, false)
                    })
            })()