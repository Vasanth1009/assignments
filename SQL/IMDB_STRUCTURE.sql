USE master;

DROP DATABASE IF EXISTS IMDB;

CREATE DATABASE IMDB;

USE IMDB;

DROP TABLE IF EXISTS Actors;

DROP TABLE IF EXISTS  Producers;

DROP TABLE IF EXISTS Movies;

DROP TABLE IF EXISTS Genres;

DROP TABLE IF EXISTS MovieActorMapping;

DROP TABLE IF EXISTS MovirProducerMapping;

DROP TABLE IF EXISTS MovieGenreMapping;

CREATE TABLE Actors(
	Id INT 
		PRIMARY KEY 
		IDENTITY,
	Name VARCHAR(255),
	Sex VARCHAR(10),
	DOB DATE,
	Bio VARCHAR(255)
);

CREATE TABLE Producers(
	Id INT 
		PRIMARY KEY 
		IDENTITY,
	Name VARCHAR(255),
	Sex VARCHAR(10),
	DOB DATE,
	Bio VARCHAR(255)
);

CREATE TABLE Movies(
	Id INT 
		PRIMARY KEY 
		IDENTITY,
	Name VARCHAR(255),
	YearOfRelease INT,
	Plot TEXT,
	Poster VARBINARY(MAX),
	ProducerId INT 
		FOREIGN KEY 
		REFERENCES 
			Producers(Id)
);

CREATE TABLE Genres(
	Id INT 
		PRIMARY KEY 
		IDENTITY,
	Name VARCHAR(255),
);

CREATE TABLE MovieActorMapping(
	MovieId INT	
		FOREIGN KEY 
		REFERENCES 
		Movies(Id),
	ActorId INT 
		FOREIGN KEY 
		REFERENCES 
			Actors(Id)
);

CREATE TABLE MovieProducerMapping(
	ProducerId INT	
		FOREIGN KEY 
		REFERENCES 
		Producers(Id),
	MovieId INT 
		FOREIGN KEY 
		REFERENCES 
			Movies(Id)
);

CREATE TABLE MovieGenreMapping(
	MovieId INT	
		FOREIGN KEY 
		REFERENCES 
		Movies(Id),
	GenreId INT 
		FOREIGN KEY 
		REFERENCES 
			Genres(Id)
);
