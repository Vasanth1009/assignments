USE master;

DROP DATABASE IF EXISTS MovieLibrary;

CREATE DATABASE MovieLibrary;

USE MovieLibrary;

DROP TABLE IF EXISTS Actors;

DROP TABLE IF EXISTS  Producers;

DROP TABLE IF EXISTS Movies;

DROP TABLE IF EXISTS MovieActorMapping;

CREATE TABLE Actors(
	Id INT 
		PRIMARY KEY 
		IDENTITY,
	Name VARCHAR(255),
	Gender VARCHAR(10),
	DOB DATE
);

INSERT INTO Actors 
VALUES 
	('Mila Kunis','Female','11/14/1986'),
	('Robert DeNiro','Male','07/10/1957'),
	('George Michael','Male','11/23/1978'),
	('Mike Scott','Male','08/06/1969'),
	('Pam Halpert','Female','09/26/1996'),
	('Dame Judi Dench','Female','04/05/1947')

CREATE TABLE Producers(
	Id INT 
		PRIMARY KEY 
		IDENTITY,
	Name VARCHAR(255),
	Company VARCHAR(100),
	CompanyEstDate DATE
);

INSERT INTO Producers 
VALUES 
	('Jeet','Disney','05/14/1998'),
	('Arun','Bull','09/11/2004'),
	('Tom','Hanks','11/03/1987'),
	('Zeshan','Male','11/14/1996'),
	('Nicole','Team Coco','09/26/1992');

Select  * From Producers
CREATE TABLE Movies(
	Id INT 
		PRIMARY KEY 
		IDENTITY,
	Name VARCHAR(255),
	Language VARCHAR(30),
	ProducerId INT
		FOREIGN KEY
		REFERENCES 
			Producers(Id),
	Profit INT,
);

INSERT INTO Movies 
VALUES
	('Rocky','English',1,'10000'),
	('Rocky','Hindi',3,'3000'),
	('Terminal','English',4,'300000'),
	('Rambo','Hindi',2,'93000'),
	('Rudy','English',5,'9600');

CREATE TABLE MovieActorMapping(
	MovieId INT	
		FOREIGN KEY 
		REFERENCES 
		Movies(Id),
	ActorId INT 
		FOREIGN KEY 
		REFERENCES 
			Actors(Id)
);

INSERT INTO MovieActorMapping
VALUES
	(1,1),
	(1,3),
	(1,5),
	(2,6),
	(2,5),
	(2,4),
	(2,2),
	(3,3),
	(3,2),
	(4,1),
	(4,6),
	(4,3),
	(5,2),
	(5,5),
	(5,3);


SELECT * FROM Actors;

SELECT * FROM Producers;

SELECT * FROM Movies;

SELECT * FROM MovieActorMapping;


-- Update Profit of all the movies by +1000 where producer name contains 'run'
UPDATE M
SET M.Profit = M.Profit + 1000
FROM Movies M
	INNER JOIN Producers P
		ON  P.Id = M.ProducerId
WHERE P.Name LIKE '%run%';

-- Duplicate movies having the same name and their count
SELECT Name ,COUNT(Name) AS MovieCount
FROM  Movies
GROUP BY Name 
HAVING COUNT(Name) > 1;

-- Oldest actor/actress for each movie
SELECT Main.Id, Main.Name, A.Name, Main.MinDob 
FROM(
	SELECT M.Id, M.Name, MIN(DOB) AS MinDOB 
	FROM MovieActorMapping MAM
		INNER JOIN Movies M 
			ON M.Id = MAM.MovieId
		INNER JOIN Actors A 
			ON A.Id = MAM.ActorId
	GROUP BY M.Id,M.Name
) AS Main
	INNER JOIN Actors A 
		ON A.DOB = MinDOB
ORDER BY Main.Id;

-- List of producers who have not worked with actor X
SELECT P.Id,P.Name,P.Company, P.CompanyEstDate
FROM Producers P
WHERE	P.Id NOT IN (
	SELECT Main.Id
	FROM (
		SELECT P.Id
		FROM Producers P
			INNER JOIN Movies M
				ON M.ProducerId = P.Id
			INNER JOIN MovieActorMapping MAM
				ON M.Id = MAM.MovieId 
			INNER JOIN Actors A
				ON A.Id = MAM.ActorId
		WHERE (A.Name = 'George Michael')
		) AS Main
);

-- List of pair of Actors who have worked together in more than 1 movies

SELECT 
(
	SELECT Name 
	FROM Actors 
	WHERE Id = M1.ActorId
) AS Actor1, M1.ActorId,
(
	SELECT Name 
	FROM Actors 
	WHERE Id = M2.ActorId
) AS Actor2, M2.ActorId,
	COUNT(M1.MovieId) AS Count
FROM MovieActorMapping M1, MovieActorMapping M2
WHERE M1.MovieId = M2.MovieId AND M1.ActorId<M2.ActorId
GROUP BY M1.ActorId,M2.ActorId
HAVING COUNT(M1.MovieId) > 1 
ORDER BY Count DESC;

-- Add non-clustered index on profit column of movies table
CREATE NONCLUSTERED INDEX IDX_Profit 
	ON Movies(Profit);
GO

sp_helpindex Movies


-- Create stored procedure to return list of actors for given movie id;
CREATE PROCEDURE ActorListByMovieId (@MovieId INT)
AS
	BEGIN
		SELECT A.Name 
		FROM Actors A
			INNER JOIN MovieActorMapping MAM 
				ON A.Id = MAM.ActorId 
		WHERE MAM.MovieId = @MovieId
	END
EXEC ActorListByMovieId @MovieId=1;
GO

-- Create a function to return age for given date of birth
CREATE FUNCTION GetAge(@DOB DATE)
returns int
AS 
	BEGIN
		RETURN YEAR(GETDATE())-YEAR(@DOB)
	END
GO


select m.name , count(mam.ActorId)
from Movies m
inner join MovieActorMapping MAM
on mam.MovieId = m.Id
group by m.name,m.id