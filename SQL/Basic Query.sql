USE master;

DROP DATABASE IF EXISTS School;

CREATE DATABASE School;

USE School;

DROP TABLE IF EXISTS Classes;

DROP TABLE IF EXISTS Teachers;

DROP TABLE IF EXISTS Students;

DROP TABLE IF EXISTS TeacherClassMapping;

CREATE TABLE Classes(
	Id INT 
		PRIMARY KEY 
		IDENTITY,
	Name VARCHAR(10),
	Section VARCHAR(2), 
	Number INT
);

INSERT INTO Classes 
VALUES 
	('IX','A',201),
	('IX','B',202),
	('X','A',203);


CREATE TABLE Teachers(
	Id INT 
		PRIMARY KEY 
		IDENTITY, 
	Name VARCHAR(100),
	DOB DATE, 
	Gender VARCHAR(10)
);

INSERT INTO Teachers 
VALUES 
	('Lisa Kudrow','1985/06/08','Female'), 
	('Monica Bing','1982/03/06','Female'), 
	('Chandler Bing','1978/12/17','Male'), 
	('Ross Geller','1993/01/26','Male')

CREATE TABLE Students(
	Id INT 
		PRIMARY KEY 
		IDENTITY, 
	Name VARCHAR(100),
	DOB DATE,
	Gender VARCHAR(10), 
	ClassId INT 
		FOREIGN KEY 
		REFERENCES 
			Classes(Id)
);

INSERT INTO Students
VALUES 
	('Scotty Loman','2006/01/31','Male',1),
	('Adam Scott','2005/06/01','Male',1), 
	('Natosha Beckles','2005/01/23','Female',2), 
	('Lilly Page','2006/11/26','Female',2), 
	('John Freeman','2006/06/14','Male',2),
	('Morgan Scott','2005/05/18','Male',3), 
	('Codi Gass','2005/12/24','Female',3),
	('Nick Roll','2005/12/24','Male',3),
	('Dave Grohl','2005/02/12','Male',3)

CREATE TABLE TeacherClassMapping(
	TeacherId INT 
		FOREIGN KEY 
		REFERENCES 
			Teachers(Id), 
	ClassId INT 
		FOREIGN KEY 
		REFERENCES 
			Classes(Id)
);

INSERT INTO TeacherClassMapping
VALUES
	(1,3),
	(1,2),
	(2,2),
	(2,3),
	(3,3),
	(3,1),
	(4,3)

SELECT * FROM Classes;

SELECT * FROM Teachers;

SELECT * FROM Students;

SELECT * FROM TeacherClassMapping;

-- Male Students
SELECT * 
FROM Students S 
WHERE S.Gender = 'Male';

-- Students older than 2005/01/01
SELECT * 
FROM Students S 
WHERE S.DOB > '2005/01/01';

-- Youngest Student in School
SELECT * 
FROM Students 
WHERE DOB = 
	(SELECT MIN(DOB) FROM Students);


-- Students Distinct BirthDays
SELECT DISTINCT S.DOB
FROM Students S;

-- No of students in each class
SELECT C.Name, Count(S.ClassId) AS ClassCount
FROM Students S
INNER JOIN Classes C
	ON C.Id = S.ClassId
GROUP BY C.Name;

-- No of students in each section
SELECT C.Name, C.Section, Count(S.ClassId) AS SectionCount
FROM Students S
INNER JOIN Classes C
	ON C.Id = S.ClassId
GROUP BY C.Name, C.Section;

-- No of classes taught by teacher
SELECT T.Id, T.Name, Count(C.Id) as ClassCount
FROM TeacherClassMapping TCM
INNER JOIN Classes C 
	ON TCM.ClassId = C.Id
RIGHT JOIN Teachers T
	ON TCM.TeacherId = T.Id
GROUP BY  T.Id,T.Name;

-- List of teachers teaching Class X
SELECT T.Name, T.DOB, T.Gender
FROM Teachers T
INNER JOIN TeacherClassMapping TCM
	ON TCM.TeacherId = T.Id
INNER JOIN Classes C
	ON TCM.ClassId = C.Id
WHERE C.Name = 'X';

-- Classes which have more than 2 teachers
SELECT C.Id, C.Name, C.Section, COUNT(TCM.TeacherId) AS TeacherCount
FROM Classes C
INNER JOIN TeacherClassMapping TCM
	ON TCM.ClassId = C.Id
WHERE C.Id IN ( 
	SELECT TCM.ClassId 
	FROM TeacherClassMapping TCM 
	GROUP BY TCM. ClassId 
	HAVING COUNT(TCM.TeacherId) > 2 
)
GROUP BY C.Id, C.Name, C.Section;

-- List of students being taught by 'Lisa'
SELECT S.Name
FROM Students S
INNER JOIN TeacherClassMapping TCM
	ON TCM.ClassId = S.ClassId
INNER JOIN Teachers T
	ON T.Id = TCM.TeacherId
WHERE T.Name LIKE 'Lisa%';