﻿using MovieLibrary.Domain;
using MovieLibrary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MovieLibrary
{
    public class LibraryService
    {
        private MovieRepo _movieRepo;
        private ActorRepo _actorRepo;
        private ProducerRepo _producerRepo;

        //Initialize MovieRepo, ActorRepo, ProducerRepo
        public LibraryService()
        {
            _movieRepo = new MovieRepo();
            _actorRepo = new ActorRepo();
            _producerRepo = new ProducerRepo();
        }

        #region Movie    
        /// <summary>Check whether actor and producer are in the list.</summary>
        public string CheckAddMovie()
        {
            var actors = _actorRepo.Get();
            var producers = _producerRepo.Get();
            var validation = "";
            if (actors.Count == 0 && producers.Count == 0)
            {
                validation = "Actor and Producer";
            }
            else if (producers.Count == 0)
            {
                validation = "Producer";
            }
            else if( actors.Count == 0)
            {
                validation = "Actor";
            }
            return validation;
        }

        /// <summary>Check whether movie is in the list.</summary>
        public bool CheckMovieCount()
        {
            var validation = true;
            var movie = _movieRepo.Get();
            if (movie.Count == 0)
            {
                validation = false;
            }
            return validation;
        }

        /// <summary>Check whether actor is available in the list or not.</summary>
        public bool CheckAddActorMovie(string selectedActor)
        {
            var actorsIds = selectedActor.Split(",");
            var validation = true;
            foreach (var val in actorsIds)
            {
                var actorVals = int.Parse(val);
                if (_actorRepo.Get().Count < actorVals)
                {
                    validation = false;
                    break;
                }
            }
            return validation;
        }

        /// <summary>Add a movie to the list.</summary>
        public bool AddMovie(string name, int yor, string plot, string actorsIdList, int producerId)
        {
            var actorsIds = actorsIdList.Split(",");
            var selectedActors = new List<Actor>();
            var actors = GetActors();
            foreach (var val in actorsIds)
            {
                var actorVals = int.Parse(val);
                selectedActors.Add(actors.ElementAt(actorVals - 1));
            }
            var producers = GetProducers();
            bool checkProducers = true;
            if (producers.Count < producerId)
            {
                checkProducers = false;
            }
            if (checkProducers)
            {
                var selectedProducer = producers.ElementAt(producerId - 1);
                var movie = new Movie()
                {
                    Name = name,
                    YearOfRelease = yor,
                    Plot = plot,
                    ActorsName = selectedActors,
                    ProducerName = selectedProducer
                };

                _movieRepo.Add(movie);
            }
            return checkProducers;
        }

        /// <summary>Display all movies in the list with details.</summary>
        public List<Movie> ShowMovies()
        {
            var movies = _movieRepo.Get();
            return movies;
        }

        /// <summary>Display all movies name in the list by actor count.</summary>
        public List<Movie> MovieByActorCount(int count)
        {
            var movies = _movieRepo.Get();
            var moviesName = movies.Where(x => x.ActorsName.Count >= count).ToList();
            return moviesName;
        }

        /// <summary>Display all movies name in the list by producer name.</summary>
        public List<Movie> MovieByProducerName(string name)
        {
            var movies = _movieRepo.Get();
            var moviesName = movies.Where(x => x.ProducerName.Name == name).ToList();
            return moviesName;
        }

        /// <summary>Display all actors name in the movie list by actor name.</summary>
        public List<Actor> ActorsByMovieName(string name)
        {
            var movies = _movieRepo.Get();
            var actorsList = movies.Where(x => x.Name == name).SelectMany(y => y.ActorsName).ToList();
            return actorsList;
        }
 
        /// <summary>Delete the movie in the list by Movie name.</summary>
        public void DeleteMovie(string movieName)
        {
            _movieRepo.Delete(movieName);
        }
        #endregion

        #region Actor
        /// <summary>Add a actor to the list.</summary>
        public void AddActor(string name, DateTime dob)
        {
            Actor actor = new Actor()
            {
                Name = name,
                DOB = dob
            };
            _actorRepo.Add(actor);
        }

        /// <summary> Get all the actors from list.</summary>
        public List<Actor> GetActors()
        {
            return _actorRepo.Get();
        }
        #endregion

        #region Producer
        /// <summary>Add a producer to the list.</summary>
        public void AddProducer(string name, DateTime dob)
        {
            Producer producer = new Producer()
            {
                Name = name,
                DOB = dob
            };
            _producerRepo.Add(producer);

        }
        /// <summary> Get all the producers from list.</summary>
        public List<Producer> GetProducers()
        {
            return _producerRepo.Get();
        }
        #endregion
    }
}
