﻿using MovieLibrary.Domain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MovieLibrary
{
    public class Program
    {
        private static LibraryService _libraryService;

        static void Main(string[] args)
        {
            //Initialize LibraryService
            _libraryService = new LibraryService();

            Console.WriteLine(" Welcome To MovieLibrary ");
            var flag = true;
            do
            {
                try
                {


                    Console.WriteLine(" Press 1 to Get Movie");
                    Console.WriteLine(" Press 2 to Add Movie");
                    Console.WriteLine(" Press 3 to Add Actor");
                    Console.WriteLine(" Press 4 to Add Producer");
                    Console.WriteLine(" Press 5 to Delete Movie");
                    Console.WriteLine(" Press 6 to Get Movie by Actor Count");
                    Console.WriteLine(" Press 7 to Get Movie by Producer Name");
                    Console.WriteLine(" Press 8 to Get All Actors by Movie Name");
                    Console.WriteLine(" Press 9 to Exit");
                    Console.WriteLine();
                    Console.WriteLine("What do you want to do?");
                    var choice = Convert.ToInt32(Console.ReadLine());
                    if (choice == 1)
                    {
                        var checkMovies = _libraryService.CheckMovieCount();
                        if (!checkMovies)
                        {
                            Console.WriteLine("***No Movie in the list please add atleast one movie***");
                            Console.WriteLine();
                        }
                        else
                        {
                            var movies = _libraryService.ShowMovies();
                            movies.ForEach(movie =>
                             {
                                 Console.WriteLine(" Movie Name :" + movie.Name);
                                 Console.WriteLine(" Release Year :" + movie.YearOfRelease);
                                 Console.WriteLine(" Plot :" + movie.Plot);

                                 Console.Write(" Actors : ");
                                 int actorCount = 1;
                                 var actors = movie.ActorsName;
                                 actors.ForEach(actor =>
                                 {
                                     Console.Write(actorCount + ") " + actor.Name + "   ");
                                     actorCount++;
                                 });
                                 Console.WriteLine();

                                 Console.Write(" Producers : ");
                                 var producer = movie.ProducerName;
                                 Console.WriteLine(producer.Name + "   ");
                                 Console.WriteLine();
                             });
                        }
                    }
                    else if (choice == 2)
                    {
                        var checkMovie = _libraryService.CheckAddMovie();
                        if (checkMovie == "Actor and Producer")
                        {
                            Console.WriteLine("***Please add atlease one Actor and Producer***");
                            Console.WriteLine();
                        }
                        else if(checkMovie == "Producer" )
                        {
                            Console.WriteLine("***Please add the atlease one Producer***");
                            Console.WriteLine();
                        }
                        else if (checkMovie == "Actor")
                        {
                            Console.WriteLine("***Please add the atlease one Actor***");
                            Console.WriteLine();
                        }
                        else {
                            string name, plot;
                            int yor;
                            Console.WriteLine("Enter Movie Name: ");
                            name = Console.ReadLine();
                            if (name != "")
                            {
                                Console.WriteLine("Enter the Year of Release: ");
                                yor = Convert.ToInt32(Console.ReadLine());
                                if (yor > 1900 && yor < 2025)
                                {
                                    Console.WriteLine("Enter Movie Plot: ");
                                    plot = Console.ReadLine();
                                    if (plot != "")
                                    {
                                        //Actor
                                        Console.WriteLine("Choose the Actors (eg: 1,2) : ");
                                        var actorCount = 1;
                                        var actors = _libraryService.GetActors();
                                        actors.ForEach(actor =>
                                        {
                                            Console.Write(actorCount + ") " + actor.Name + "   ");
                                            actorCount++;
                                        });
                                        Console.WriteLine();
                                        var selectedActors = Console.ReadLine();
                                        var checkActor = _libraryService.CheckAddActorMovie(selectedActors);

                                        if (!checkActor)
                                        {
                                            Console.WriteLine("****Please enter the valid actor number****");
                                            Console.WriteLine();
                                        }
                                        else { 
                                            //Producer
                                            Console.WriteLine("Choose the Producer(Only one): ");
                                            var producerCount = 1;
                                            var producers = _libraryService.GetProducers();
                                            producers.ForEach(producer =>
                                            {
                                                Console.Write(producerCount + ") " + producer.Name + "   ");
                                                producerCount++;
                                            });
                                            Console.WriteLine();
                                            var selectedProducer = Convert.ToInt32(Console.ReadLine());

                                            var  checkAddMovie = _libraryService.AddMovie(name, yor, plot, selectedActors, selectedProducer);
                                            if (!checkAddMovie)
                                            {
                                                Console.WriteLine("****Please enter the valid producer number****");
                                                Console.WriteLine();
                                            }
                                            else
                                            {
                                                Console.WriteLine("<<<Movie Added Successfully>>>");
                                                Console.WriteLine();
                                            }
                                        }

                                    }
                                    else
                                    {
                                        Console.WriteLine("****Please enter a movie plot don't leave it empty****");
                                        Console.WriteLine();
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("****Please enter the valid year between 1900 and 2025****");
                                    Console.WriteLine();
                                }
                            }
                            else
                            {
                                Console.WriteLine("****Please enter a movie name don't leave it empty****");
                                Console.WriteLine();
                            }
                        }
                    }
                    else if (choice == 3)
                    {
                        string name;
                        string dob;
                        Console.WriteLine("Enter the actor name: ");
                        name = Console.ReadLine();
                        if (name == "")
                        {
                            Console.WriteLine("****Please enter a actor name don't leave it empty****");
                            Console.WriteLine();
                        }
                        else
                        {
                            Console.WriteLine("Enter the Date of Birth (dd/MM/yyyy)");
                            var validDate = true;
                            try
                            {
                                dob = Console.ReadLine();
                                DateTime dateTimeObj = DateTime.ParseExact(dob, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                var currentDate = DateTime.Today;

                                if (dob != "" && ((currentDate.Year - dateTimeObj.Year) < 18 || (currentDate.Year - dateTimeObj.Year) > 80))
                                {
                                    validDate = false;
                                    Console.WriteLine("**** Age should be greater than 18 and less than 80****");
                                }
                                else
                                {
                                    _libraryService.AddActor(name, dateTimeObj);
                                }
                            }
                            catch (Exception)
                            {
                                validDate = false;
                                Console.WriteLine("****Please enter a date in valid format****");
                            }
                            if (validDate)
                                Console.WriteLine("<<<Actor Added Successfully>>>");
                            Console.WriteLine();
                        }
                    }
                    else if (choice == 4)
                    {
                        string name;
                        string dob;
                        Console.WriteLine("Enter the producer name: ");
                        name = Console.ReadLine();
                        if (name == "")
                        {
                            Console.WriteLine("****Please enter a producer name don't leave it empty****");
                            Console.WriteLine();
                        }
                        else
                        {
                            Console.WriteLine("Enter the Date of Birth (dd/MM/yyyy)");
                            var validDate = true;
                            try
                            {
                                dob = Console.ReadLine();
                                DateTime dateTimeObj = DateTime.ParseExact(dob, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                var currentDate = DateTime.Today;

                                if (dob != "" && ((currentDate.Year - dateTimeObj.Year) < 18 || (currentDate.Year - dateTimeObj.Year) > 80))
                                {
                                    validDate = false;
                                    Console.WriteLine("**** Age should be greater than 18 and less than 80****");
                                }
                                else
                                {
                                    _libraryService.AddProducer(name, dateTimeObj);
                                }

                            }
                            catch (Exception)
                            {
                                validDate = false;
                                Console.WriteLine("****Please enter a date in valid format****");
                            }
                            if (validDate)
                                Console.WriteLine("<<<Producer Added Successfully>>>");
                            Console.WriteLine();
                        }
                    }
                    else if (choice == 5)
                    {
                        var check = _libraryService.CheckMovieCount();
                        if (!check)
                        {
                            Console.WriteLine("***No Movie in the list please add atleast one movie***");
                            Console.WriteLine();
                        }
                        else
                        {
                            Console.WriteLine("Enter the movie name to delete :");
                            var movieName = Console.ReadLine();
                            if (movieName == "")
                            {
                                Console.WriteLine("****Please enter a movie name don't leave it empty****");
                                Console.WriteLine();
                            }
                            else
                            {
                                _libraryService.DeleteMovie(movieName);
                                Console.WriteLine();
                            }
                        }
                    }
                    else if (choice == 6)
                    {
                        var check = _libraryService.CheckMovieCount();
                        if (!check)
                        {
                            Console.WriteLine("***No Movie in the list please add atleast one movie***");
                            Console.WriteLine();
                        }
                        {
                            Console.WriteLine("Enter the Actor Count : ");
                            var count = Convert.ToInt32(Console.ReadLine());
                            var moviesName = _libraryService.MovieByActorCount(count);
                            if( moviesName.Count != 0)
                            {

                                var movieCount = 1;
                                moviesName.ForEach(movieName =>
                                {
                                    Console.WriteLine(movieCount + ") " + movieName.Name);
                                    movieCount++;
                                });
                            }
                            else
                            {
                                Console.WriteLine("****No movies have more than " + count + " actors in the list.****");
                            }
                            Console.WriteLine();
                        }

                    }
                    else if (choice == 7)
                    {
                        var check = _libraryService.CheckMovieCount();
                        if (!check)
                        {
                            Console.WriteLine("***No Movie in the list please add atleast one movie***");
                            Console.WriteLine();
                        }
                        else
                        {
                            Console.WriteLine("Enter the Producer Name : ");
                            var producerName = Console.ReadLine();
                            var moviesName = _libraryService.MovieByProducerName(producerName);
                            if (moviesName.Count != 0)
                            {
                                var movieCount = 1;
                                moviesName.ForEach(name =>
                                {
                                    Console.WriteLine(movieCount + ") " + name.Name);
                                    movieCount++;
                                });
                            }
                            else
                            {
                                Console.WriteLine("****No movies in the list have producer name as " + producerName + ".****");
                            }
                            Console.WriteLine();
                        }
                    }
                    else if(choice == 8)
                    {
                        var check = _libraryService.CheckMovieCount();
                        if (!check)
                        {
                            Console.WriteLine("***No Movie in the list please add atleast one movie***");
                            Console.WriteLine();
                        }
                        else
                        {
                            Console.WriteLine("Enter the Movie Name : ");
                            var movieName = Console.ReadLine();
                            var actorsList = _libraryService.ActorsByMovieName(movieName);
                            if (actorsList.Count != 0)
                            {
                                var actorsCount = 1;
                                actorsList.ForEach( name =>
                                {

                                    Console.WriteLine(actorsCount + ") " + name.Name);
                                    actorsCount++;
                                });
                            }
                            else
                            {
                                Console.WriteLine("****No movies in the list have movie name as " + movieName + ".****");
                            }
                            Console.WriteLine();
                        }
                    }
                    else if (choice == 9)
                    {
                        flag = false;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("****Please enter in valid format****");
                    Console.WriteLine();
                }

            } while (flag);

        }
    }
}
