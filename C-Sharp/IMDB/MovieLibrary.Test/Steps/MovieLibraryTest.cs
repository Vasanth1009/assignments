﻿using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace MovieLibrary.Test.Steps
{
    [Binding]
    public sealed class MovieLibraryTest
    {
        private string _name, _plot, _actorName, _producerName;
        private int _yor;
        private DateTime _dob;
        private string _actors;
        private int _producer;
        private LibraryService _libraryService;

        public MovieLibraryTest()
        {
            _libraryService = new LibraryService();
        }

        [Given(@"movie name is '(.*)'")]
        public void GivenMovieNameIs(string name)
        {
            _name = name;
        }

        [Given(@"year is '(.*)'")]
        public void GivenYearIs(int year)
        {
            _yor = year;
        }

        [Given(@"plot is '(.*)'")]
        public void GivenPlotIs(string plot)
        {
            _plot = plot;
        }

        [Given(@"actor name is '(.*)'")]
        public void GivenActorNameIs(string actors)
        {
            _actors = actors;
        }

        [Given(@"producer name is '(.*)'")]
        public void GivenProducerNameIs(int producer)
        {

            _producer = producer;
        }

        [When(@"add movie to the empty table")]
        public void WhenAddMovieToTheEmptyTable()
        {
            _libraryService.AddMovie(_name, _yor, _plot, _actors, _producer);
        }

        [Then(@"table is look like")]
        public void ThenTableIsLookLike(Table table)
        {
            table.CompareToSet(_libraryService.ShowMovies());
        }

        [When(@"add movie to the non-empty table")]
        public void WhenAddMovieToTheNon_EmptyTable()
        {
            _libraryService.AddMovie(_name, _yor, _plot, _actors, _producer);
        }

        [Then(@"table is look like a")]
        public void ThenTableIsLookLikeA(Table table)
        {
            table.CompareToSet(_libraryService.ShowMovies());
        }


        [When(@"get the movies from table")]
        public void WhenGetTheMoviesFromTable()
        {
            _libraryService.ShowMovies();
        }

        [Then(@"show the movies like")]
        public void ThenShowTheMoviesLike(Table table)
        {
            table.CompareToSet(_libraryService.ShowMovies());
        }

        [When(@"delete movie from the non-empty table")]
        public void WhenDeleteMovieFromTheNon_EmptyTable()
        {
            _libraryService.DeleteMovie(_name);
        }

        [Then(@"table is look as")]
        public void ThenTableIsLookAs(Table table)
        {
            table.CompareToSet(_libraryService.ShowMovies());
        }


        [Given(@"actorname is '(.*)'")]
        public void GivenActornameIs(string actors)
        {
            _actorName = actors;
        }


        [Given(@"dob is '(.*)'")]
        public void GivenDobIs(DateTime dob)
        {
            _dob = dob;
        }

        [When(@"add actor to the empty table")]
        public void WhenAddActorToTheEmptyTable()
        {
            _libraryService.AddActor(_actorName, _dob);
        }

        [Then(@"actor table is look like")]
        public void ThenActorTableIsLookLike(Table table)
        {
            table.CompareToSet(_libraryService.GetActors());
        }

        [Then(@"producer table is look like")]
        public void ThenProducerTableIsLookLike(Table table)
        {
            table.CompareToSet(_libraryService.GetProducers());
        }

        [Given(@"producername is '(.*)'")]
        public void GivenProducernameIs(string producers)
        {
            _producerName = producers;
        }

        [When(@"add producer to the empty table")]
        public void WhenAddProducerToTheEmptyTable()
        {
            _libraryService.AddProducer(_producerName, _dob);
        }

        [BeforeScenario("add-empty-movie")]
        public void AddMovieEmptyScenarioPrerequisites()
        {
            _libraryService.AddActor("ABC", DateTime.Parse("10/09/1999"));
            _libraryService.AddProducer("GHI", DateTime.Parse("10/09/1999"));
        }

        [BeforeScenario("add-non-empty-movie")]
        public void AddMovieScenarioPrerequisites()
        {
            _libraryService.AddActor("ABC", DateTime.Parse("10/09/1999"));
            _libraryService.AddActor("DEF", DateTime.Parse("10/09/1999"));
            _libraryService.AddActor("GHI", DateTime.Parse("10/09/1999"));
            _libraryService.AddActor("JKL", DateTime.Parse("10/09/1999"));
            _libraryService.AddActor("MNO", DateTime.Parse("10/09/1999"));
            _libraryService.AddProducer("XYZ", DateTime.Parse("10/09/1999"));
            _libraryService.AddProducer("UVW", DateTime.Parse("10/09/1999"));
            _libraryService.AddMovie("Harry Potter", 2016, "Kill the Enemy", "1,3", 2);
        }

        [BeforeScenario("get-movies")]
        public void ListMovieScenarioPrerequisties()
        {
            _libraryService.AddActor("ABC", DateTime.Parse("10/09/1999"));
            _libraryService.AddActor("DEF", DateTime.Parse("10/09/1999"));
            _libraryService.AddActor("GHI", DateTime.Parse("10/09/1999"));
            _libraryService.AddActor("JKL", DateTime.Parse("10/09/1999"));
            _libraryService.AddActor("MNO", DateTime.Parse("10/09/1999"));
            _libraryService.AddProducer("XYZ", DateTime.Parse("10/09/1999"));
            _libraryService.AddProducer("UVW", DateTime.Parse("10/09/1999"));
            _libraryService.AddMovie("Harry Potter", 2016, "Kill the Enemy", "1,3", 1);
            _libraryService.AddMovie("Harry Potter 2", 2019, "Kill the Enemy", "4,5", 2);
            //_libraryService.ShowMovies();

        }
        [BeforeScenario("delete-movie")]
        public void DeleteMovieScenarioPrerequisites()
        {
            _libraryService.AddActor("ABC", DateTime.Parse("10/09/1999"));
            _libraryService.AddProducer("XYZ", DateTime.Parse("10/09/1999"));
            _libraryService.AddMovie("Harry Potter", 2016, "Kill the Enemy", "1", 1);
            _libraryService.AddMovie("Harry Potter 2", 2019, "Kill the Enemy", "1", 1);
            _libraryService.DeleteMovie("Harry Potter");

        }
    }
}
