﻿Feature: MovieLibrary
	To manage a movies in application

@add-empty-movie
Scenario: Add new movie in empty database
	Given movie name is 'Harry Potter'
	And year is '2016'
	And plot is 'Kill the Enemy'
	And actor name is '1'
	And producer name is '1'
	When add movie to the empty table
	Then table is look like
	| Name         | YearOfRelease | Plot           | 
	| Harry Potter | 2016          | Kill the Enemy | 
	And actor table is look like
	| Name | DOB        |
	| ABC  | 10/09/1999 |
	And producer table is look like
	| Name | DOB        |
	| GHI  | 10/09/1999 |


@add-non-empty-movie
Scenario: Add new movie in non-empty database
	Given movie name is 'Harry Potter 2'
	And year is '2019'
	And plot is 'Kill the Enemy'
	And actor name is '4,5'
	And producer name is '2'
	When add movie to the non-empty table
	Then table is look like a
	| Name           | YearOfRelease | Plot           | 
	| Harry Potter   | 2016          | Kill the Enemy |
	| Harry Potter 2 | 2019          | Kill the Enemy |
	And actor table is look like
	| Name | DOB        |
	| ABC  | 10/09/1999 |
	| DEF  | 10/09/1999 |
	| GHI  | 10/09/1999 |
	| JKL  | 10/09/1999 |
	| MNO  | 10/09/1999 |
	And producer table is look like
	| Name | DOB        |
	| XYZ  | 10/09/1999 |
	| UVW  | 10/09/1999 |

@get-movies
Scenario: Get movies in non-empty database
	When get the movies from table
	Then show the movies like
	| Name           | YearOfRelease | Plot           | 
	| Harry Potter   | 2016          | Kill the Enemy |
	| Harry Potter 2 | 2019          | Kill the Enemy |
	And actor table is look like
	| Name | DOB        |
	| ABC  | 10/09/1999 |
	| DEF  | 10/09/1999 |
	| JKL  | 10/09/1999 |
	| MNO  | 10/09/1999 |
	| GHI  | 10/09/1999 |
	And producer table is look like
	| Name | DOB        |
	| UVW  | 10/09/1999 |
	| XYZ  | 10/09/1999 |

@delete-movie
Scenario: Delete movie in non-empty database
	When delete movie from the non-empty table
	Then table is look as
	| Name           | YearOfRelease | Plot           |
	| Harry Potter 2 | 2019          | Kill the Enemy |
	And actor table is look like
	| Name | DOB        |
	| ABC  | 10/09/1999 |
	And producer table is look like
	| Name | DOB        |
	| XYZ  | 10/09/1999 |


@add-actor
Scenario: Add new actor in empty database
	Given actorname is 'Harry'
	And dob is '09/10/1989'
	When add actor to the empty table
	Then actor table is look like 
	| Name  | DOB        |
	| Harry | 09/10/1989 |

@add-producer
Scenario: Add new producer in empty database
	Given producername is 'Harry'
	And dob is '09/10/1989'
	When add producer to the empty table
	Then producer table is look like 
	| Name  | DOB        |
	| Harry | 09/10/1989 |
