﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieLibrary.Domain
{
    public class Person
    {
        public string Name{ get; set; }
        public DateTime DOB { get; set; }
    }
}
