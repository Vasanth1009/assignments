﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieLibrary.Domain
{
    public class Movie
    {

        public string Name { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }
        public List<Actor> ActorsName { get; set; }
        public Producer ProducerName { get; set; }

    }
}
