﻿using MovieLibrary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MovieLibrary.Repository
{
    public class ProducerRepo
    {
        private List<Producer> _producers;

        public ProducerRepo()
        {
            _producers = new List<Producer>();
        }

        public void Add(Producer producer)
        {
            _producers.Add(producer);
        }

        public List<Producer> Get()
        {
            return _producers.ToList();
        }
    }
}
