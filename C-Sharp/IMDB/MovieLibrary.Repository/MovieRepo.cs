﻿using MovieLibrary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MovieLibrary.Repository
{
    public class MovieRepo
    {
        private List<Movie> _movies;
        public MovieRepo()
        {
            _movies = new List<Movie>();
        }

        public void Add(Movie movie)
        {
            _movies.Add(movie);
        }

        public List<Movie>  Get()
        {
            return _movies.ToList();
        }

        public void Delete(string movieName)
        {
            var mov =  _movies.FirstOrDefault(m => m.Name == movieName);
            if( mov== null )
            {
                Console.WriteLine("****Movie does not exist please enter a valid name****");
            }
            else
            {
                _movies.Remove(mov);
                Console.WriteLine("<<<Movie Deleted Successfully>>>");
            }
        }
    }
}
