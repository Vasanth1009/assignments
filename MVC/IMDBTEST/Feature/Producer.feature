﻿Feature: Producer Resource

@GetAllProducers
Scenario: Get All Producer
	Given I am a client
	When I make GET Request '/api/producer'
	Then response code must be '200'
	And response data must look like '[{"id":5,"name":"Allirajah Subaskaran","gender":"Male","dob":"1972-03-02T00:00:00","bio":"Darbar Producer"}]'

@GetProduderById
Scenario: Get Producer
	Given I am a client
	When I make GET Request '/api/producer/5'
	Then response code must be '200'
	And response data must look like '{"id":5,"name":"Allirajah Subaskaran","gender":"Male","dob":"1972-03-02T00:00:00","bio":"Darbar Producer"}'

@AddProducer
Scenario:Add Producer
	Given I am a client
	When I am making a POST request to '/api/producer' with the following Data '{"Name":"Xavier Britto","Gender":"Male","DOB":"1965-02-17","Bio":"Master Producer"}'
	Then response code must be '200'

@UpdateProducer
Scenario: Update Producer
	Given I am a client
	When I make PUT Request '/api/producer/1' with the following Data '{"Name":"Xavier Britto","Gender":"Male","DOB":"1965-03-17","Bio":"Master Producer"} '
	Then response code must be '200'

@RemoveProducer
Scenario:Remove Producer
	Given I am a client
	When I make Delete Request '/api/producer/1'
	Then response code must be '200'