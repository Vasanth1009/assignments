﻿Feature: Movie Resource

@GetAllMovies
Scenario: Get All Movie
	Given I am a client
	When I make GET Request '/api/movie'
	Then response code must be '200'
	And response data must look like '[{"id":6,"name":"Darbar","yearOfRelease":2020,"plot":"Aaditya, the commissioner of Mumbai Police, sets out to catch Ajay, a drug peddler. However, he uncovers a deeper controversy linked to an international drug lord and must mete out justice.","producer":{"id":5,"name":"Allirajah Subaskaran","gender":"Male","dob":"1972-03-02T00:00:00","bio":"Darbar Producer"},"poster":"https://en.wikipedia.org/wiki/File:Darbar_release_poster.jpg","actors":[{"id":3,"name":"Rajini","gender":"Male","dob":"1950-12-12T00:00:00","bio":"Super Star"},{"id":11,"name":"Nayanthara","gender":"Female","dob":"1986-02-22T00:00:00","bio":"Lady Super Star"}],"genres":[{"id":2,"name":"Action"},{"id":7,"name":"Romance"}]}]'

@GetMovieById
Scenario: Get Movie
	Given I am a client
	When I make GET Request '/api/movie/6'
	Then response code must be '200'
	And response data must look like '{"id":6,"name":"Darbar","yearOfRelease":2020,"plot":"Aaditya, the commissioner of Mumbai Police, sets out to catch Ajay, a drug peddler. However, he uncovers a deeper controversy linked to an international drug lord and must mete out justice.","producer":{"id":5,"name":"Allirajah Subaskaran","gender":"Male","dob":"1972-03-02T00:00:00","bio":"Darbar Producer"},"poster":"https://en.wikipedia.org/wiki/File:Darbar_release_poster.jpg","actors":[{"id":3,"name":"Rajini","gender":"Male","dob":"1950-12-12T00:00:00","bio":"Super Star"},{"id":11,"name":"Nayanthara","gender":"Female","dob":"1986-02-22T00:00:00","bio":"Lady Super Star"}],"genres":[{"id":2,"name":"Action"},{"id":7,"name":"Romance"}]}'

@AddMovie
Scenario:Add Movie
	Given I am a client
	When  I am making a POST request to '/api/movie' with the following Data '{"Name":"Master","yearOfRelease":2021,"plot":"JD, an alcoholic professor, is enrolled to teach at a juvenile facility, unbeknownst to him. He soon clashes with a ruthless gangster, who uses the children as scapegoats for his crimes.","producers":1,"poster":"https://en.wikipedia.org/wiki/File:Master_2021_poster.jpg","actors":"1,2","genres":"1"}'
	Then response code must be '200'

@UpdateMovie
Scenario: Update Movie
	Given I am a client
	When I make PUT Request '/api/movie/1' with the following Data '{"Name":"Master","yearOfRelease":2021,"plot":"JD, is an alcoholic professor, is enrolled to teach at a juvenile facility, unbeknownst to him. He soon clashes with a ruthless gangster, who uses the children as scapegoats for his crimes.","producers":1,"poster":"https://en.wikipedia.org/wiki/File:Master_2021_poster.jpg","actors":"1,3","genres":"1,2"}'
	Then response code must be '200'

@RemoveMovie
Scenario:Remove Movie
	Given I am a client
	When I make Delete Request '/api/movie/1'
	Then response code must be '200'
