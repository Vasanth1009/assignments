﻿Feature: Actor Resource

@GetAllActors
Scenario: Get All Actors
	Given I am a client
	When I make GET Request '/api/actor'
	Then response code must be '200'
	And response data must look like '[{"id":8,"name":"Surya","gender":"Male","dob":"1979-03-02T00:00:00","bio":"Tamil Star"},{"id":2,"name":"Vijay","gender":"Male","dob":"1973-06-22T00:00:00","bio":"Thalapathy"}]'

@GetActorById
Scenario: Get Actor
	Given I am a client
	When I make GET Request '/api/actor/8'
	Then response code must be '200'
	And response data must look like '{"id":8,"name":"Surya","gender":"Male","dob":"1979-03-02T00:00:00","bio":"Tamil Star"}'

@AddActor
Scenario:Add Actor
	Given I am a client
	When  I am making a POST request to '/api/actor' with the following Data '{"Name":"Vijay","Gender":"Male","DOB":"1973-06-22","Bio":"Thalapathy"}'
	Then response code must be '200'

@UpdateActor
Scenario: Update Actor
	Given I am a client
	When I make PUT Request '/api/actor/8' with the following Data '{"Name":"Vijay","Gender":"Male","DOB":"1973-06-22","Bio":"Thalapathi"}'
	Then response code must be '200'

@RemoveActor
Scenario:Remove Actor
	Given I am a client
	When I make Delete Request '/api/actor/8'
	Then response code must be '200'