﻿Feature: Genre Resource

@GetAllGenres/
Scenario: Get All Genres
	Given I am a client
	When I make GET Request '/api/genre'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Comedy"},{"id":2,"name":"Action"}]'

@GetGenreById
Scenario: Get Genre
	Given I am a client
	When I make GET Request '/api/genre/2'
	Then response code must be '200'
	And response data must look like '{"id":2,"name":"Action"}'

@AddGenre
Scenario:Add Genre
	Given I am a client
	When  I am making a POST request to '/api/genre' with the following Data '{ "Name":"Comedy" }'
	Then response code must be '200'

@UpdateGenre
Scenario: Update Genre
	Given I am a client
	When I make PUT Request '/api/genre/2' with the following Data '{ "Name":"Thriller" }'
	Then response code must be '200'

@RemoveGenre
Scenario:Remove Genre 
	Given I am a client
	When I make Delete Request '/api/genre/1'
	Then response code must be '200'