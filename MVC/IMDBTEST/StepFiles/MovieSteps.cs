﻿using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
using IMDBAPI;
using IMDB.Test;
using IMDB.Test.MockResources;

namespace IMDB.Test.StepFiles
{
    [Scope(Feature = "Movie Resource")]
    [Binding]
    public class MovieSteps : BaseSteps
    {
        public MovieSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => MovieMock.movieRepoMock.Object);
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            MovieMock.MockGetAll();
            MovieMock.MockGet();
            MovieMock.MockAdd();
            MovieMock.MockUpdate();
            MovieMock.MockRemove();
        }
    }
}