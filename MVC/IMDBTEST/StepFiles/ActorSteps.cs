﻿using IMDB.Test.MockResources;
using IMDBAPI;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
using IMDBAPI.Services;
using IMDBAPI.Services.Service_Interface;

namespace IMDB.Test.StepFiles
{
    [Scope(Feature = "Actor Resource")]
    [Binding]
    public class ActorSteps : BaseSteps
    {
        public ActorSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => ActorMock.actorRepoMock.Object);
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            ActorMock.MockGetAll();
            ActorMock.MockGet();
            ActorMock.MockAdd();
            ActorMock.MockUpdate();
            ActorMock.MockRemove();
        }

    }
}