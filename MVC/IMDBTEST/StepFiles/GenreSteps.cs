﻿using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
using IMDBAPI;
using IMDBAppTest.MockResources;

namespace IMDB.Test.StepFiles
{
    [Scope(Feature = "Genre Resource")]
    [Binding]
    public class GenreSteps : BaseSteps
    {
        public GenreSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => GenreMock.genreRepoMock.Object);

                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            GenreMock.MockGetAll();
            GenreMock.MockGet();
            GenreMock.MockAdd();
            GenreMock.MockUpdate();
            GenreMock.MockRemove();
        }
    }
}
