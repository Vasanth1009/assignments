﻿using System;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
using IMDBAppTest.MockResources;
using IMDBAPI;
using IMDB.Test.MockResources;

namespace IMDB.Test.StepFiles
{
    [Scope(Feature = "Producer Resource")]
    [Binding]
    public class ProducerSteps : BaseSteps
    {
        public ProducerSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    // Producer Repo
                    services.AddScoped(service => ProducerMock.producerRepoMock.Object);

                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            ProducerMock.MockGetAll();
            ProducerMock.MockGet();
            ProducerMock.MockAdd();
            ProducerMock.MockUpdate();
            ProducerMock.MockRemove();
        }
    }
}
