﻿using IMDBAPI.Models;
using IMDBAPI.Repository.Repository_Interface;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace IMDB.Test.MockResources
{
    public class ActorMock
    {
        public static readonly Mock<IActorRepository> actorRepoMock = new Mock<IActorRepository>();

        public static void MockGetAll()
        {
            actorRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfActors());
        }

        public static void MockGet()
        {
            actorRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) => ListOfActors().SingleOrDefault(x => x.Id == id));
        }

        public static void MockAdd()
        {
            actorRepoMock.Setup(repo => repo.Add(It.IsAny<Actor>()));
        }

        public static void MockUpdate()
        {
            actorRepoMock.Setup(repo => repo.Update(It.IsAny<int>(), It.IsAny<Actor>()));
        }

        public static void MockRemove()
        {
            actorRepoMock.Setup(repo => repo.Remove(It.IsAny<int>()));
        }

        private static IEnumerable<Actor> ListOfActors()
        {
            var list = new List<Actor>()
            {
                new Actor()
                {
                    Id=8,
                    Name="Surya",
                    Gender="Male",
                    Bio="Tamil Star",
                    DOB=new DateTime(1979,03,02)
                },
                new Actor()
                {
                    Id=2,
                    Name="Vijay",
                    Gender="Male",
                    Bio="Thalapathy",
                    DOB=new DateTime(1973,06,22)
                }
            };
            return list;
        }
        public static List<int> MovieActorMapping(int movieId)
        {
            var movieActorsMapping = new Dictionary<int, List<int>>
                {
                    {1, new List<int> {1}}
                };
            return movieActorsMapping[movieId];
        }
        public static void GetActorByMovieId()
        {
            actorRepoMock.Setup(repo => repo.GetByMovie(It.IsAny<int>())).Returns((int movieId) =>
            {
                var actorsId = MovieActorMapping(movieId);
                return ListOfActors().Where(actor => actorsId.Contains(actor.Id));
            });
        }
    }
}