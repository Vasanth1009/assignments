﻿using IMDBAPI.Models;
using IMDBAPI.Repository.Repository_Interface;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace IMDB.Test.MockResources
{
    public class MovieMock
    {
        public static readonly Mock<IMovieRepository> movieRepoMock = new Mock<IMovieRepository>();


        public static void MockGetAll()
        {
            movieRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfMovies());
        }

        public static void MockGet()
        {
            movieRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) => ListOfMovies().SingleOrDefault(x => x.Id == id));
        }

        public static void MockAdd()
        {
            movieRepoMock.Setup(repo => repo.Add(It.IsAny<Movie>(), It.IsAny<List<int>>(), It.IsAny<List<int>>()));
        }

        public static void MockUpdate()
        {
            movieRepoMock.Setup(repo => repo.Update(It.IsAny<int>(), It.IsAny<Movie>(), It.IsAny<List<int>>(), It.IsAny<List<int>>()));
        }

        public static void MockRemove()
        {
            movieRepoMock.Setup(repo => repo.Remove(It.IsAny<int>()));
        }

        private static IEnumerable<Movie> ListOfMovies()
        {
            var list = new List<Movie>() {
                new Movie()
                {
                    Id=6,
                    Name="Darbar",
                    YearOfRelease=2020,
                    Plot="Aaditya, the commissioner of Mumbai Police, sets out to catch Ajay, a drug peddler. However, he uncovers a deeper controversy linked to an international drug lord and must mete out justice.",
                    Poster="https://en.wikipedia.org/wiki/File:Darbar_release_poster.jpg",
                    ProducerId = 5
                }

            };
            return list;
        }

    }
}
