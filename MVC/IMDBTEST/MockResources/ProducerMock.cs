﻿using IMDBAPI.Models;
using IMDBAPI.Repository.Repository_Interface;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDBAppTest.MockResources
{
    public class ProducerMock
    {
        public static readonly Mock<IProducerRepository> producerRepoMock = new Mock<IProducerRepository>();


        public static void MockGetAll()
        {
            producerRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfProducers());
        }

        public static void MockGet()
        {
            producerRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) => ListOfProducers().SingleOrDefault(x => x.Id == id));
        }

        public static void MockAdd()
        {
            producerRepoMock.Setup(repo => repo.Add(It.IsAny<Producer>()));
        }

        public static void MockUpdate()
        {
            producerRepoMock.Setup(repo => repo.Update(It.IsAny<int>(), It.IsAny<Producer>()));
        }

        public static void MockRemove()
        {
            producerRepoMock.Setup(repo => repo.Remove(It.IsAny<int>()));
        }

        private static List<Producer> ListOfProducers()
        {
            var list = new List<Producer>() {
                new Producer()
                {
                    Id=5,
                    Name="Allirajah Subaskaran",
                    Gender="Male",
                    DOB=new DateTime(1972,03,02),
                    Bio="Darbar Producer"
                }

            };
            return list;
        }
    }
}

