﻿using IMDBAPI.Models;
using IMDBAPI.Repository.Repository_Interface;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace IMDBAppTest.MockResources
{
    public class GenreMock
    {
        public static readonly Mock<IGenreRepository> genreRepoMock = new Mock<IGenreRepository>();


        public static void MockGetAll()
        {
            genreRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfGenres()); ;
        }

        public static void MockGet()
        {
            genreRepoMock.Setup(repo => repo.Get(It.IsAny<int>())).Returns((int id) => ListOfGenres().SingleOrDefault(x => x.Id == id));
        }
        
        public static void MockAdd()
        {
            genreRepoMock.Setup(repo => repo.Add(It.IsAny<Genre>()));
        }

        public static void MockUpdate()
        {
            genreRepoMock.Setup(repo => repo.Update(It.IsAny<int>(), It.IsAny<Genre>()));
        }

        public static void MockRemove()
        {
            genreRepoMock.Setup(repo => repo.Remove(It.IsAny<int>()));
        }

        private static IEnumerable<Genre> ListOfGenres()
        {
            var list = new List<Genre>() {
                new Genre()
                {
                    Id=1,
                    Name="Comedy"
                },
                new Genre()
                {
                    Id=2,
                    Name="Action"
                }
            };
            return list;
        }
        public static List<int> MovieGenresMapping(int movieId)
        {
            var movieGenresMapping = new Dictionary<int, List<int>>
            {
                {1, new List<int>{1} }
            };
            return movieGenresMapping[movieId];
        }

        public static void GetGenreByMovieId()
        {
            genreRepoMock.Setup(repo => repo.GetByMovie(It.IsAny<int>())).Returns((int movieId) =>
            {
                var genresId = MovieGenresMapping(movieId);
                return ListOfGenres().Where(genre => genresId.Contains(genre.Id));
            });
        }

    }
}
