﻿using IMDBAPI.Models;
using IMDBAPI.Models.Requests;
using IMDBAPI.Models.Responses;
using IMDBAPI.Repository.Repository_Interface;
using IMDBAPI.Services.Service_Interface;
using System.Collections.Generic;
using System.Linq;

namespace IMDBAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IProducerRepository _producerRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IGenreRepository _genreRepository;

        public MovieService(IMovieRepository movieRepository, IProducerRepository producerRepository,
                IActorRepository actorRepository, IGenreRepository genreRepository)
        {
            _movieRepository = movieRepository;
            _producerRepository = producerRepository;
            _actorRepository = actorRepository;
            _genreRepository = genreRepository;
        }

        public IEnumerable<MovieResponse> GetAll()
        {
            var movies = new List<MovieResponse>();
            var movieModel = _movieRepository.GetAll().ToList();
            movieModel.ForEach(movie =>
            {
                var producer = _producerRepository.Get(movie.ProducerId);
                var actors = _actorRepository.GetByMovie(movie.Id);
                var genres = _genreRepository.GetByMovie(movie.Id);
                var movieResponse = new MovieResponse()
                {
                    Id = movie.Id,
                    Name = movie.Name,
                    YearOfRelease = movie.YearOfRelease,
                    Plot = movie.Plot,
                    Producer = producer,
                    Poster = movie.Poster,
                    Actors = actors,
                    Genres = genres
                };
                movies.Add(movieResponse);
            });
            return movies;
        }

        public MovieResponse Get(int id)
        {
            var movie = _movieRepository.Get(id);
            if(movie == null)
            {
                return null;
            }
            var producer = _producerRepository.Get(movie.ProducerId);
            if (producer == null)
            {
                return null;
            }
            var actors = _actorRepository.GetByMovie(movie.Id);
            if (actors == null)
            {
                return null;
            }
            var genres = _genreRepository.GetByMovie(movie.Id);
            if (genres == null)
            {
                return null;
            }
            var movieResponse = new MovieResponse()
            {
                Id = movie.Id,
                Name = movie.Name,
                YearOfRelease = movie.YearOfRelease,
                Plot = movie.Plot,
                Producer = producer,
                Poster = movie.Poster,
                Actors = actors,
                Genres = genres
            };
            return movieResponse;
        }
  
        public MovieResponse Add(MovieRequest movieRequest)
        {
            var movie = new Movie()
            {
                Name = movieRequest.Name,
                YearOfRelease = movieRequest.YearOfRelease,
                Plot = movieRequest.Plot,
                ProducerId = movieRequest.ProducerId,
                Poster = movieRequest.Poster,
            };
            var movieId = _movieRepository.Add(movie, movieRequest.ActorsId, movieRequest.GenresId);
            var producer = _producerRepository.Get(movie.ProducerId);
            var actors = _actorRepository.GetByMovie(movieId);
            var genres = _genreRepository.GetByMovie(movieId);
            var movieResponse = new MovieResponse()
            {
                Id = movieId,
                Name = movie.Name,
                YearOfRelease = movie.YearOfRelease,
                Plot = movie.Plot,
                Producer = producer,
                Poster = movie.Poster,
                Actors = actors,
                Genres = genres
            };
            return movieResponse;
        }

        public MovieResponse Update(int id, MovieRequest movieRequest)
        {
            var movie = new Movie()
            {
                Name = movieRequest.Name,
                Plot = movieRequest.Plot,
                YearOfRelease = movieRequest.YearOfRelease,
                ProducerId = movieRequest.ProducerId,
                Poster = movieRequest.Poster
            };
            _movieRepository.Update(id, movie, movieRequest.ActorsId, movieRequest.GenresId);
            var producer = _producerRepository.Get(movie.ProducerId);
            var actors = _actorRepository.GetByMovie(id);
            var genres = _genreRepository.GetByMovie(id);
            var movieResponse = new MovieResponse()
            {
                Id = id,
                Name = movie.Name,
                YearOfRelease = movie.YearOfRelease,
                Plot = movie.Plot,
                Producer = producer,
                Poster = movie.Poster,
                Actors = actors,
                Genres = genres
            };
            return movieResponse;
        }

        public void Remove(int id)
        {
            _movieRepository.Remove(id);
        }



    }
}
