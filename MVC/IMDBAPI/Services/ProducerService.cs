﻿using IMDBAPI.Models;
using IMDBAPI.Models.Requests;
using IMDBAPI.Models.Responses;
using IMDBAPI.Repository.Repository_Interface;
using IMDBAPI.Services.Service_Interface;
using System.Collections.Generic;
using System.Linq;

namespace IMDBAPI.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;

        public ProducerService(IProducerRepository producerRepository)
        {
            _producerRepository = producerRepository;
        }

        public IEnumerable<ProducerResponse> GetAll()
        {
            var producers = new List<ProducerResponse>();

            var producerModel = _producerRepository.GetAll().ToList();
            foreach (var producer in producerModel)
            {
                var producerResponse = new ProducerResponse()
                {
                    Id = producer.Id,
                    Name = producer.Name,
                    Gender = producer.Gender,
                    DOB = producer.DOB,
                    Bio = producer.Bio
                };
                producers.Add(producerResponse);
            }

            return producers;
        }

        public ProducerResponse Get(int id)
        {
            var producer = _producerRepository.Get(id);
            if(producer == null)
            {
                return null; 
            }
            var producerResponse = new ProducerResponse()
            {
                Id = producer.Id,
                Name = producer.Name,
                Gender = producer.Gender,
                DOB = producer.DOB,
                Bio = producer.Bio
            };
            return producerResponse;
        }

        public ProducerResponse Add(ProducerRequest producerRequest)
        {

            var producer = new Producer()
            {
                Name = producerRequest.Name,
                Gender = producerRequest.Gender,
                DOB = producerRequest.DOB,
                Bio = producerRequest.Bio
            };
            var producerId = _producerRepository.Add(producer);
            var producerResponse = new ProducerResponse()
            {
                Id = producerId,
                Name = producer.Name,
                Gender = producer.Gender,
                DOB = producer.DOB,
                Bio = producer.Bio
            };
            return producerResponse;
        }

        public ProducerResponse Update(int id, ProducerRequest producerRequest)
        {
            var producer = new Producer()
            {
                Name = producerRequest.Name,
                Gender = producerRequest.Gender,
                DOB = producerRequest.DOB,
                Bio = producerRequest.Bio
            };
            _producerRepository.Update(id, producer);
            var producerResponse = new ProducerResponse()
            {
                Id = id,
                Name = producer.Name,
                Gender = producer.Gender,
                DOB = producer.DOB,
                Bio = producer.Bio
            };
            return producerResponse;
        }

        public void Remove(int id)
        {
            _producerRepository.Remove(id);
        }


    }
}
