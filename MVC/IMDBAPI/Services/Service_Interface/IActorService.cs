﻿using IMDBAPI.Models.Requests;
using IMDBAPI.Models.Responses;
using System.Collections.Generic;

namespace IMDBAPI.Services.Service_Interface
{
    public interface IActorService
    {
        public IEnumerable<ActorResponse> GetAll();

        public ActorResponse Get(int id);

        public ActorResponse Add(ActorRequest actorRequest);

        public ActorResponse Update(int id, ActorRequest actorRequest);

        public void Remove(int id);



    }
}
