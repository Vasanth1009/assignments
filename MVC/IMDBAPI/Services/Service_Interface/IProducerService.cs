﻿using IMDBAPI.Models.Requests;
using IMDBAPI.Models.Responses;
using System.Collections.Generic;

namespace IMDBAPI.Services.Service_Interface
{
    public interface IProducerService
    {
        public IEnumerable<ProducerResponse> GetAll();

        public ProducerResponse Get(int id);

        public ProducerResponse Add(ProducerRequest producerRequest);

        public ProducerResponse Update(int id, ProducerRequest producerRequest);

        public void Remove(int id);

       
    }
}