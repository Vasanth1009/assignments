﻿using IMDBAPI.Models.Requests;
using IMDBAPI.Models.Responses;
using System.Collections.Generic;

namespace IMDBAPI.Services.Service_Interface
{
    public interface IMovieService
    {
        public IEnumerable<MovieResponse> GetAll();

        public MovieResponse Get(int id);

        public MovieResponse Add(MovieRequest movieRequest);

        public MovieResponse Update(int id, MovieRequest movieRequest);

        public void Remove(int id);

    }
}
