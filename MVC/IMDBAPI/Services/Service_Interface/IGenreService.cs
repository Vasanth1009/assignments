﻿using IMDBAPI.Models.Requests;
using IMDBAPI.Models.Responses;
using System.Collections.Generic;

namespace IMDBAPI.Services.Service_Interface
{
    public interface IGenreService
    {
        public IEnumerable<GenreResponse> GetAll();

        public GenreResponse Get(int id);

        public GenreResponse Add(GenreRequest genreRequest);

        public GenreResponse Update(int id, GenreRequest genreRequest);

        public void Remove(int id);


    }
}