﻿using IMDBAPI.Models;
using IMDBAPI.Models.Requests;
using IMDBAPI.Models.Responses;
using IMDBAPI.Repository.Repository_Interface;
using IMDBAPI.Services.Service_Interface;
using System.Collections.Generic;
using System.Linq;

namespace IMDBAPI.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;

        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
        }

        public IEnumerable<GenreResponse> GetAll()
        {
            var genres = new List<GenreResponse>();

            var genreModel = _genreRepository.GetAll().ToList();
            genreModel.ForEach(genre =>
            {
                var genreResponse = new GenreResponse()
                {
                    Id = genre.Id,
                    Name = genre.Name,
                };
                genres.Add(genreResponse);
            });

            return genres;
        }

        public GenreResponse Get(int id)
        {
            var genre = _genreRepository.Get(id);
            if(genre == null)
            {
                return null;
            }
            var genreResponse = new GenreResponse()
            {
                Id = genre.Id,
                Name = genre.Name,
            };
            return genreResponse;
        }

        public GenreResponse Add(GenreRequest genreRequest)
        {
            var genre = new Genre()
            {
                Name = genreRequest.Name,
            };
            var genreId = _genreRepository.Add(genre);
            var genreResponse = new GenreResponse()
            {
                Id = genreId,
                Name = genre.Name,
            };
            return genreResponse;
        }

        public GenreResponse Update(int id, GenreRequest genreRequest)
        {
            var genre = new Genre()
            {
                Name = genreRequest.Name,
            };
            _genreRepository.Update(id, genre);
            var genreResponse = new GenreResponse()
            {
                Id = id,
                Name = genre.Name,
            };
            return genreResponse;
        }

        public void Remove(int id)
        {
            _genreRepository.Remove(id);
        }


    }
}
