﻿using IMDBAPI.Models;
using IMDBAPI.Models.Requests;
using IMDBAPI.Models.Responses;
using IMDBAPI.Repository.Repository_Interface;
using IMDBAPI.Services.Service_Interface;
using System.Collections.Generic;
using System.Linq;

namespace IMDBAPI.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;

        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }

        public IEnumerable<ActorResponse> GetAll()
        {
            var actors = new List<ActorResponse>();

            var actorModel = _actorRepository.GetAll().ToList();
            actorModel.ForEach(actor =>
            {
                var actorResponse = new ActorResponse()
                {
                    Id = actor.Id,
                    Name = actor.Name,
                    Gender = actor.Gender,
                    DOB = actor.DOB,
                    Bio = actor.Bio
                };
                actors.Add(actorResponse);
            });
            return actors;
        }

        public ActorResponse Get(int id)
        {
            var actor = _actorRepository.Get(id);
            if (actor == null)
            {
                return null;
            }
            var actorResponse = new ActorResponse()
            {
                Id = actor.Id,
                Name = actor.Name,
                Gender = actor.Gender,
                DOB = actor.DOB,
                Bio = actor.Bio
            };
            return actorResponse;
        }

        public ActorResponse Add(ActorRequest actorRequest)
        {

            var actor = new Actor()
            {
                Name = actorRequest.Name,
                Gender = actorRequest.Gender,
                DOB = actorRequest.DOB,
                Bio = actorRequest.Bio
            };
            var actorId = _actorRepository.Add(actor);
            var actorResponse = new ActorResponse()
            {
                Id = actorId,
                Name = actor.Name,
                Gender = actor.Gender,
                DOB = actor.DOB,
                Bio = actor.Bio
            };
            return actorResponse;
        }

        public ActorResponse Update(int id,ActorRequest actorRequest)
        {
            var actor = new Actor()
            {
                Name = actorRequest.Name,
                Gender = actorRequest.Gender,
                DOB = actorRequest.DOB,
                Bio = actorRequest.Bio
            };
            _actorRepository.Update(id, actor);
            var actorResponse = new ActorResponse()
            {
                Id = id,
                Name = actor.Name,
                Gender = actor.Gender,
                DOB = actor.DOB,
                Bio = actor.Bio
            };
            return actorResponse;
        }

        public void Remove(int id)
        {
            _actorRepository.Remove(id);
        }


    }
}
