﻿using Dapper;
using IMDBAPI.Models;
using IMDBAPI.Repository.Repository_Interface;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace IMDBAPI.Repository
{
    public class ProducerRepository : IProducerRepository
    {
        private readonly ConnectionString _connectionString;
        public ProducerRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }

        public IEnumerable<Producer> GetAll()
        {
            var sql = @"
SELECT Id,
    Name,
    Gender,
    DOB,
    Bio  
FROM Producers";
            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                return connection.Query<Producer>(sql);
            }
        }

        public Producer Get(int id)
        {
            var sql = @"
SELECT Id,
    Name,
    Gender,
    DOB,
    Bio  
FROM Producers 
WHERE Id = @ProducerId";
            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                return connection.QuerySingleOrDefault<Producer>(sql, new { ProducerId = id });
            }
        }

        public int Add(Producer producer)
        {
            var sql = @"
INSERT INTO Producers 
VALUES(
    @Name, 
    @Gender, 
    @DOB, 
    @Bio)
SELECT CAST(SCOPE_IDENTITY() as int)";
            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                return connection.ExecuteScalar<int>(sql, producer);
            }
        }

        public void Update(int id, Producer producer)
        {
            var sql = @"
UPDATE Producers 
SET Name = @Name, 
    Gender = @Gender, 
    DOB = @DOB, 
    Bio = @Bio 
WHERE Id = @Id";
            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                connection.Execute(sql, new
                {
                    producer.Name,
                    producer.DOB,
                    producer.Bio,
                    producer.Gender,
                    Id = id
                });
            }
        }

        public void Remove(int id)
        {
            var sql = @"
DELETE 
FROM Producers 
WHERE Id = @ProducerId";
            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                connection.Execute(sql, new { ProducerId = id });
            }
        }


    }
}
