﻿using Dapper;
using IMDBAPI.Models;
using IMDBAPI.Repository.Repository_Interface;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace IMDBAPI.Repository
{
	public class MovieRepository : IMovieRepository
	{
		private readonly ConnectionString _connectionString;
		public MovieRepository(IOptions<ConnectionString> connectionString)
		{
			_connectionString = connectionString.Value;
		}

		public IEnumerable<Movie> GetAll()
		{
			var sql = @"
SELECT Id
	,Name
	,YearOfRelease
	,Plot
	,ProducerId
	,Poster
FROM Movies";
			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				return connection.Query<Movie>(sql).ToList();
			}
		}

		public Movie Get(int id)
		{
			var sql = @"
SELECT Id
	,Name
	,YearOfRelease
	,Plot
	,ProducerId
	,Poster
FROM Movies
WHERE Id = @MovieId";
			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				return connection.QuerySingleOrDefault<Movie>(sql, new { MovieId = id });
			}
		}

		public int Add(Movie movie, List<int> actorsId, List<int> genresId)
		{
			var sql = @"AddMovie";
			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				return connection.ExecuteScalar<int>(sql, new {
					movie.Name,
					movie.YearOfRelease,
					movie.Plot,
					movie.ProducerId,
					movie.Poster,
					ActorsId = string.Join(",", actorsId),
					GenresId = string.Join(",", genresId)
				}, commandType: CommandType.StoredProcedure);

			}
		}

		public void Update(int id, Movie movie, List<int> actorsId, List<int> genresId)
		{
			var sql = @"UpdateMovie";
			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				connection.Execute(sql, new
				{
					movie.Name,
					movie.YearOfRelease,
					movie.Plot,
					movie.ProducerId,
					movie.Poster,
					ActorsId = string.Join(",", actorsId),
					GenresId = string.Join(",", genresId),
					Id = id
				}, commandType: CommandType.StoredProcedure);
			}
		}

		public void Remove(int id)
		{
			var sql = @"
DELETE 
FROM Movies 
WHERE Id = @MovieId";
			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				connection.Execute(sql, new { MovieId = id });
			}
		}



	}
}
