﻿using IMDBAPI.Models;
using System.Collections.Generic;

namespace IMDBAPI.Repository.Repository_Interface
{
    public interface IMovieRepository
    {
        public IEnumerable<Movie> GetAll();

        public Movie Get(int id);

        public int Add(Movie movie, List<int> actorsId, List<int> genresId);

        public void Update(int id, Movie movie, List<int> actorsId, List<int> genresId);

        public void Remove(int id);



    }
}
