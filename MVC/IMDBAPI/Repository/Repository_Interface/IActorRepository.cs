﻿using IMDBAPI.Models;
using System.Collections.Generic;

namespace IMDBAPI.Repository.Repository_Interface
{
    public interface IActorRepository
    {
        public IEnumerable<Actor> GetAll();

        public Actor Get(int id);

        public IEnumerable<Actor> GetByMovie(int id);

        public int Add(Actor actor);

        public void Update(int id, Actor actor);

        public void Remove(int id);



    }
}
