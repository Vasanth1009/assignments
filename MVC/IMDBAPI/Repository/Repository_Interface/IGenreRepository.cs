﻿using IMDBAPI.Models;
using System.Collections.Generic;

namespace IMDBAPI.Repository.Repository_Interface
{
    public interface IGenreRepository
    {
        public IEnumerable<Genre> GetAll();

        public Genre Get(int id);

        public IEnumerable<Genre> GetByMovie(int id);

        public int Add(Genre genre);

        public void Update(int id, Genre genre);

        public void Remove(int id);


    }
}
