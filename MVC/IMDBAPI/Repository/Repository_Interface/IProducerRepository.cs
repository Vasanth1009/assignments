﻿using IMDBAPI.Models;
using System.Collections.Generic;

namespace IMDBAPI.Repository.Repository_Interface
{
    public interface IProducerRepository
    {
        public IEnumerable<Producer> GetAll();

        public Producer Get(int id);

        //public Producer GetByMovie(int id);

        public int Add(Producer producer);

        public void Update(int id, Producer producer);

        public void Remove(int id);

    }
}
