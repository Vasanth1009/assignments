﻿using Dapper;
using IMDBAPI.Models;
using IMDBAPI.Repository.Repository_Interface;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace IMDBAPI.Repository
{
    public class GenreRepository : IGenreRepository
    {

        private readonly ConnectionString _connectionString;
        public GenreRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }

        public IEnumerable<Genre> GetAll()
        {
            var sql = @"
SELECT Id,
    Name
FROM Genres";
            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                return connection.Query<Genre>(sql);
            }
        }

        public Genre Get(int id)
        {
            var sql = @"
SELECT Id,
    Name
FROM Genres 
WHERE Id = @GenreId";
            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                return connection.QuerySingleOrDefault<Genre>(sql, new { GenreId = id });
            }
        }

        public IEnumerable<Genre> GetByMovie(int Id)
        {
            var sql = @"
SELECT G.Id
      ,G.Name
FROM Genres G
INNER JOIN Movie_Genre_Mapping MGM ON G.Id = MGM.GenreId
WHERE MGM.MovieId = @Id";

            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                return connection.Query<Genre>(sql, new { Id });
            }

        }

        public int Add(Genre genre)
        {
            var sql = @"
INSERT INTO Genres 
VALUES (@Name)
SELECT CAST(SCOPE_IDENTITY() as int)";
            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                return connection.ExecuteScalar<int>(sql, genre);
            }
        }

        public void Update(int id, Genre genre)
        {
            var sql = @"
UPDATE Genres 
SET Name = @Name 
WHERE Id = @Id";
            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                connection.Execute(sql, new
                {
                    genre.Name,
                    Id = id
                });
            }
        }

        public void Remove(int id)
        {
            var sql = @"
DELETE 
FROM Genres 
WHERE Id = @GenreId";
            using (var connection = new SqlConnection(_connectionString.DefaultConnection))
            {
                connection.Execute(sql, new { GenreId = id });
            }
        }



    }
}
