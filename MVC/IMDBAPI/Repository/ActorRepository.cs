﻿using Dapper;
using IMDBAPI.Models;
using IMDBAPI.Repository.Repository_Interface;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace IMDBAPI.Repository
{
	public class ActorRepository : IActorRepository
	{
		private readonly ConnectionString _connectionString;
		public ActorRepository(IOptions<ConnectionString> connectionString)
		{
			_connectionString = connectionString.Value;
		}

		public  IEnumerable<Actor> GetAll()
		{
			var sql = @"
SELECT Id,
	Name,
	Gender,
	DOB,
	Bio
FROM Actors";
			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				return connection.Query<Actor>(sql);
			}
		}

		public Actor Get(int id)
		{
			var sql = @"
SELECT Id,
	Name,
	Gender,
	DOB,
	Bio 
FROM Actors 
WHERE Id = @ActorId";
			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				return connection.QuerySingleOrDefault<Actor>(sql, new { ActorId = id });
			}
		}

		public IEnumerable<Actor> GetByMovie(int Id)
		{
			var sql = @"
SELECT A.Id
	,A.Name
	,A.Gender
	,A.DOB
	,A.Bio
FROM Actors A
INNER JOIN Movie_Actor_Mapping MAM ON A.Id = MAM.ActorId
WHERE MAM.MovieId = @Id";

			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				return connection.Query<Actor>(sql, new { Id });
			}
		}


		public int Add(Actor actor)
		{
			var sql = @"
INSERT INTO Actors 
VALUES(
	@Name, 
	@Gender, 
	@DOB, 
	@Bio);
SELECT CAST(SCOPE_IDENTITY() as int)";
			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				return connection.ExecuteScalar<int>(sql, actor );
			}
		}

		public void Update(int id, Actor actor)
		{
			var sql = @"
UPDATE Actors 
SET Name = @Name, 
	Gender = @Gender, 
	DOB = @DOB, 
	Bio = @Bio 
WHERE Id = @Id";
			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				connection.ExecuteScalar<int>(sql, new
				{
					actor.Name,
					actor.DOB,
					actor.Bio,
					actor.Gender,
					Id = id
				});
			}
		}

		public void Remove(int id)
		{
			var sql = @"
DELETE 
FROM Actors 
WHERE Id = @ActorId";
			using (var connection = new SqlConnection(_connectionString.DefaultConnection))
			{
				connection.Execute(sql, new { ActorId = id });
			}
		}
	}
}
