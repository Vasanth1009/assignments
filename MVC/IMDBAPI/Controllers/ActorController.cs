﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IMDBAPI.Models;
using IMDBAPI.Services.Service_Interface;
using System.Net;
using IMDBAPI.Models.Responses;
using IMDBAPI.Models.Requests;

namespace IMDBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorController : ControllerBase
    {
        private readonly IActorService _actorService;

        public ActorController(IActorService actorService)
        {
            _actorService = actorService;
        }

        [HttpGet]
        public IActionResult GetActors()
        {
            var actors = _actorService.GetAll();
            return Ok(actors);
        }

        [HttpGet("{id}")]
        public IActionResult GetActor(int id)
        {
            var actor = _actorService.Get(id);
            return Ok(actor);
        }

        [HttpPost]
        public IActionResult PostActor(ActorRequest actorRequest)
        {
            var actor = _actorService.Add(actorRequest);
            return Ok(actor);
        }

        [HttpPut("{id}")]
        public IActionResult PutActor(int id, ActorRequest actorRequest)
        {

            var actor = _actorService.Update(id, actorRequest);
            return Ok(actor);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteActor(int id)
        {
            _actorService.Remove(id);
            return Ok("Actor Deleted");
        }
    }
}
