﻿using IMDBAPI.Models;
using IMDBAPI.Models.Requests;
using IMDBAPI.Services.Service_Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Storage;

namespace IMDBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        public IActionResult GetMovies()
        {
            var movies =  _movieService.GetAll();
            return Ok(movies);
        }

        [HttpGet("{id}")]
        public IActionResult GetMovie(int id)
        {
            var movie = _movieService.Get(id);
            return Ok(movie);
        }

        [HttpPost]
        public IActionResult PostMovie(MovieRequest movieRequest)
        {
            var movie = _movieService.Add(movieRequest);
            return Ok(movie);
        }

        [HttpPut("{id}")]
        public IActionResult PutMovie(int id, MovieRequest movieRequest)
        {
            var movie =  _movieService.Update(id, movieRequest);
            return Ok(movie);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteMovie(int id)
        {
            _movieService.Remove(id);      
            return Ok("Movie Deleted");
        }

        [HttpPost("upload")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                return Content("file not selected");

            }
            var task = await new FirebaseStorage("imdb-14a08.appspot.com")
                    .Child(Guid.NewGuid().ToString() + ".jpg")
                    .PutAsync(file.OpenReadStream());
                    return Ok(task);
        }

    }
}
