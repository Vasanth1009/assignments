﻿using IMDBAPI.Models;
using IMDBAPI.Models.Requests;
using IMDBAPI.Services.Service_Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMDBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProducerController : ControllerBase
    {
        private readonly IProducerService _producerService;

        public ProducerController(IProducerService producerService)
        {
            _producerService = producerService;
        }

        [HttpGet]
        public IActionResult GetProducers()
        {
            var producers = _producerService.GetAll();
            return Ok(producers);
        }

        [HttpGet("{id}")]
        public IActionResult GetProducer(int id)
        {
            var producer = _producerService.Get(id);
            return Ok(producer);
        }

        [HttpPost]
        public IActionResult PostProducer(ProducerRequest producerRequest)
        {
            var producer = _producerService.Add(producerRequest);
            return Ok(producer);
        }

        [HttpPut("{id}")]
        public IActionResult PutProducer(int id, ProducerRequest producerRequest)
        {
            var producer = _producerService.Update(id, producerRequest);
            return Ok(producer);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProducer(int id)
        {
            _producerService.Remove(id);
            return Ok("Producer Deleted");
        }
    }
}
