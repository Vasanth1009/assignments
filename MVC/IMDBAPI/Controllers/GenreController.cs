﻿using IMDBAPI.Models;
using IMDBAPI.Models.Requests;
using IMDBAPI.Services.Service_Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMDBAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenreController : ControllerBase
    {
        private readonly IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet]
        public IActionResult GetGenres()
        {
            var genres = _genreService.GetAll();
            return Ok(genres);
        }

        [HttpGet("{id}")]
        public IActionResult GetGenre(int id)
        {
            var genre = _genreService.Get(id);
            return Ok(genre);
        }

        [HttpPost]
        public IActionResult PostGenre(GenreRequest genreRequest)
        {
            var genre = _genreService.Add(genreRequest);
            return Ok(genre);
        }

        [HttpPut("{id}")]
        public IActionResult PutGenre(int id, GenreRequest genreRequest)
        {

            var genre = _genreService.Update(id, genreRequest);
            return Ok(genre);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteGenre(int id)
        {

            _genreService.Remove(id);
            return Ok("Genre Deleted");
        }
    }
}
