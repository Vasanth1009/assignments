USE [IMDBAPI]
GO

CREATE PROCEDURE [dbo].[UpdateMovie] @Name NVARCHAR(MAX)
	,@YearOfRelease INT
	,@Plot NVARCHAR(MAX)
	,@ProducerId INT
	,@Poster NVARCHAR(MAX)
	,@ActorsId NVARCHAR(MAX)
	,@GenresId NVARCHAR(MAX)
	,@Id INT
AS
BEGIN
 UPDATE Movies
 SET Name = @Name,
   YearOfRelease = @YearOfRelease,
   Plot = @Plot,
	ProducerId = @ProducerId,
	Poster = @Poster
WHERE Id = @Id

DELETE FROM [dbo].[Movie_Actor_Mapping]
WHERE MovieId = @Id

INSERT INTO [dbo].[Movie_Actor_Mapping] (
		MovieId
		,ActorId
		)
	SELECT @Id [MovieId]
		,[value] [ActorId]
	FROM string_split(@ActorsId, ',')

DELETE FROM [dbo].[Movie_Genre_Mapping]
WHERE MovieId = @Id

INSERT INTO [dbo].[Movie_Genre_Mapping] (
		MovieId
		,GenreId
		)
	SELECT @Id [MovieId]
		,[value] [GenreId]
	FROM string_split(@GenresId, ',')
END
