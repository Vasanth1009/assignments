USE [IMDBAPI]
GO 

CREATE PROCEDURE [dbo].[AddMovie] @Name NVARCHAR(MAX)
	,@YearOfRelease INT
	,@Plot NVARCHAR(MAX)
	,@ProducerId INT
	,@Poster NVARCHAR(MAX)
	,@ActorsId NVARCHAR(MAX)
	,@GenresId NVARCHAR(MAX)
AS
BEGIN
	DECLARE @MovieId INT

	INSERT INTO [dbo].[Movies] (
		Name
		,YearOfRelease
		,Plot
		,ProducerId
		,Poster
		)
	VALUES (
		@Name
		,@YearOfRelease
		,@Plot
		,@ProducerId
		,@Poster
		)

	SET @MovieId = SCOPE_IDENTITY()

	INSERT INTO [dbo].[Movie_Actor_Mapping] (
		MovieId
		,ActorId
		)
	SELECT @MovieId [MovieId]
		,[value] [ActorId]
	FROM string_split(@ActorsId, ',')

	INSERT INTO [dbo].[Movie_Genre_Mapping] (
		MovieId
		,GenreId
		)
	SELECT @MovieId [MovieId]
		,[value] [GenreId]
	FROM string_split(@GenresId, ',')

	SELECT @MovieId
END
