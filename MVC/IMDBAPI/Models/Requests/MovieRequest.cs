﻿using System.Collections.Generic;

namespace IMDBAPI.Models.Requests
{
    public class MovieRequest
    {
        public string Name { get; set; }

        public int YearOfRelease { get; set; }

        public string Plot { get; set; }

        public int ProducerId { get; set; }

        public string Poster { get; set; }

        public List<int> ActorsId { get; set; }

        public List<int> GenresId { get; set; }


    }
}
