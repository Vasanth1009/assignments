﻿using System;

namespace IMDBAPI.Models.Requests
{
    public class PersonRequest
    {
        public string Name { get; set; }

        public string Gender { get; set; }

        public DateTime DOB { get; set; }

        public string Bio { get; set; }
    }
}
