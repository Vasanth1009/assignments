﻿namespace IMDBAPI.Models.Requests
{
    public class GenreRequest
    {
        public string Name { get; set; }
    }
}
