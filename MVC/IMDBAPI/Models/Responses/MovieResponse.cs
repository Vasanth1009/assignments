﻿using System.Collections.Generic;

namespace IMDBAPI.Models.Responses
{
    public class MovieResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int YearOfRelease { get; set; }

        public string Plot { get; set; }

        public Producer Producer { get; set; }

        public string Poster { get; set; }

        public IEnumerable<Actor> Actors { get; set; }

        public IEnumerable<Genre> Genres { get; set; }
    }
}
