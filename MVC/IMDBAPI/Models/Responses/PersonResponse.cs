﻿using System;

namespace IMDBAPI.Models.Responses
{
    public class PersonResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public DateTime DOB { get; set; }

        public string Bio { get; set; }
    }
}
