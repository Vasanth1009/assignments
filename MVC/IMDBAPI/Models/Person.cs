﻿using System;

namespace IMDBAPI.Models
{
    public class Person
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public DateTime DOB { get; set; }

        public string Bio { get; set; }
    }
}
