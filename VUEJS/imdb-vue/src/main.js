import Vue from 'vue';
import App from './App.vue';
import router from './router';
import VueProgressBar from 'vue-progressbar';
import VueToast from 'vue-toast-notification';
import { store } from './store/store';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(VueToast, {
    position: 'top-right',
    duration: 2000,
});

Vue.use(VueProgressBar, {
    color: '#47d78a',
    failedColor: '#f7471c',
    height: '3px',
    transition: {
        speed: '0.5s',
        opacity: '0.6s',
        termination: 500,
    },
});

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
