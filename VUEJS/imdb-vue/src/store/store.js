import Vue from 'vue';
import Vuex from 'vuex';
import ActorService from '../services/ActorService';
import ProducerService from '../services/ProducerService.js';
import GenreService from '../services/GenreService.js';
import MovieService from '../services/MovieService.js';

Vue.use(Vuex);
export const store = new Vuex.Store({
    state: {
        actorsList: [],
        producersList: [],
        genresList: [],
        moviesList: [],
    },
    mutations: {
        getActors: (state, actorsList) => {
            state.actorsList = actorsList;
        },
        getProducers: (state, producersList) => {
            state.producersList = producersList;
        },
        getGenres: (state, genresList) => {
            state.genresList = genresList;
        },
        getMovies: (state, moviesList) => {
            state.moviesList = moviesList;
        },
    },
    actions: {
        getActors: ({ commit }) => {
            ActorService.getActors()
                .then((response) => {
                    commit('getActors', response.data);
                })
                .catch(() => {
                    console.log('Failed to fetch actors');
                    this.$toast.error('Failed to fetch actors.');
                });
        },
        getProducers: ({ commit }) => {
            ProducerService.getProducers()
                .then((response) => {
                    commit('getProducers', response.data);
                })
                .catch(() => {
                    console.log('Failed to fetch producers');
                    this.$toast.error('Failed to fetch producers.');
                });
        },
        getGenres: ({ commit }) => {
            GenreService.getGenres()
                .then((response) => {
                    commit('getGenres', response.data);
                })
                .catch(() => {
                    console.log('Failed to fetch genres');
                    this.$toast.error('Failed to fetch genres.');
                });
        },
        getMovies: ({ commit }) => {
            MovieService.getMovies()
                .then((response) => {
                    commit('getMovies', response.data);
                })
                .catch(() => {
                    console('Failed to fetch movies.');
                    this.$toast.error('Failed to fetch movies.');
                });
        },
    },
});
