import http from '../http-common';

class ProducerService {
    async getProducers() {
        return await http.get('producer/');
    }
    async getProducer(id) {
        return await http.get('producer/' + id);
    }
    async addProducer(producer) {
        return await http.post('producer/', producer);
    }
    async updateProducer(id, producer) {
        return await http.put('producer/' + id, producer);
    }
    async deleteProducer(id) {
        return await http.delete('producer/' + id);
    }
}

export default new ProducerService();
