import http from '../http-common';

class ActorService {
    async getActors() {
        return await http.get('actor/');
    }
    async getActor(id) {
        return await http.get('actor/' + id);
    }
    async addActor(actor) {
        return await http.post('actor/', actor);
    }
    async updateActor(id, actor) {
        return await http.put('actor/' + id, actor);
    }
    async deleteActor(id) {
        return await http.delete('actor/' + id);
    }
}

export default new ActorService();
