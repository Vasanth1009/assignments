import http from '../http-common';

class MovieService {
    async getMovies() {
        return await http.get('movie/');
    }
    async getMovie(id) {
        return await http.get('movie/' + id);
    }
    async addMovie(movie) {
        return await http.post('movie/', movie);
    }
    async updateMovie(id, movie) {
        return await http.put('movie/' + id, movie);
    }
    async deleteMovie(id) {
        return await http.delete('movie/' + id);
    }
    async addPoster(formData) {
        return await http.post('movie/upload/', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        });
    }
}

export default new MovieService();
