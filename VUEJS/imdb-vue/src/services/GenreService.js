import http from '../http-common';

class GenreService {
    async getGenres() {
        return await http.get('genre/');
    }
    async addGenre(genre) {
        return await http.post('genre/', genre);
    }
}

export default new GenreService();
